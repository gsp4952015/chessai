#pragma once

#include "Types.h"

#include <mutex>
#include <future>
#include <atomic>

namespace chess {

	class player_input_interface {
	private:
		mutable std::mutex mtx;
		std::promise<std::string> prom_requestInput;
		bool wantsInput;
		piece_color inputColor;

	public:
		player_input_interface();

		bool needsInput() const;
		piece_color colorThatNeedsInput() const;

		// returns weather or not our input was pushed to the requester(s)
		bool pushInput(const std::string& inputString);

		std::shared_future<std::string> requestInput(piece_color color);
	};

}