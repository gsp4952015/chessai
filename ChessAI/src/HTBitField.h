#pragma once

#include <memory>
#include <cassert>
#include <type_traits>
#include <algorithm>
#include <iostream>

#pragma warning( push )
#pragma warning( disable: 4800 )

template<bool B>
struct __BoolIndirection {
	static const bool value = B;
};

template<typename T>
struct _DoNotDeduce {
	typedef T type;
};
template<typename T>
using _DoNotDeduce_t = typename _DoNotDeduce<T>::type;

#define _HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS typename DataAccessType = DataAccessType,bool ___indIsSingleOperationAccess = IsSingleOperationAccess,typename = std::enable_if_t<___indIsSingleOperationAccess>
#define _HTBF_ENABLE_IF_NOT_SINGLE_OPERATION_ACCESS typename DataAccessType = DataAccessType,bool ___indIsSingleOperationAccess = IsSingleOperationAccess,typename = std::enable_if_t<!___indIsSingleOperationAccess>

// TODO
/*
	Add the ability to have a BitFieldType be a member of another BitFieldType.
	Perhaps by adding another template variable for the type's bit offset and have
	it default to 0, and have a auto-deducing template equality operator and use
	my MemCpyBits function.
	*/
//////////////////////////////////////////////////////////////////////////

// Helper functions
//////////////////////////////////////////////////////////////////////////
template<typename T>
inline T signedRShift(const T& data,int shift) {
	if(shift > 0)
		return data >> shift;
	else
		return data << -shift;
}

template<typename T>
inline T signedLShift(const T& data,int shift) {
	if(shift > 0)
		return data << shift;
	else
		return data >> -shift;
}

// NOTE: this function can only copy up to a single byte at a time, so use it appropriately
inline char* memcpy_bits(char* const dest,size_t dest_bit,const char* const source,size_t source_bit,size_t num) {
	while(num > 0) {
		const char dest_bitPositionInByte = dest_bit & 0x7;
		const char source_bitPositionInByte = source_bit & 0x7;
		const size_t bitsToCopy = (std::min)(num,size_t(8 - (dest_bitPositionInByte > source_bitPositionInByte ? dest_bitPositionInByte : source_bitPositionInByte)));
		char* const dest_byte = dest + (dest_bit >> 3);
		const char* const source_byte = source + (source_bit >> 3);
		const char dest_mask = ~(0xff << dest_bitPositionInByte) | ~(0xff >> (8 - bitsToCopy - dest_bitPositionInByte));
		*dest_byte &= dest_mask; // clear target bits
		const char shiftAmountToAlign = source_bitPositionInByte - dest_bitPositionInByte;
		*dest_byte |= ~dest_mask & (shiftAmountToAlign >= 0 ? (*source_byte >> shiftAmountToAlign) : (*source_byte << -shiftAmountToAlign)); // copy target bits from source

		dest_bit += bitsToCopy;
		source_bit += bitsToCopy;
		num -= bitsToCopy;
	}

	return dest;
}

// Meta-functions
//////////////////////////////////////////////////////////////////////////
template<int Number,int Min,int Max,bool Inclusive = false>
struct is_int_between {
	static const bool value = Inclusive ? (Number >= Min && Number <= Max) : (Number > Min && Number < Max);
};

// determine accessor type at runtime
//////////////////////////////////////////////////////////////////////////
template <int MaximumBits,class Enable = void>
struct _HTBitFieldAccessorType { typedef void type; };

template <int MaximumBits>
struct _HTBitFieldAccessorType<MaximumBits,typename std::enable_if_t<is_int_between<MaximumBits,1,8,true>::value>> { typedef unsigned char type; };
template <int MaximumBits>
struct _HTBitFieldAccessorType<MaximumBits,typename std::enable_if_t<is_int_between<MaximumBits,9,16,true>::value>> { typedef unsigned short int type; };
template <int MaximumBits>
struct _HTBitFieldAccessorType<MaximumBits,typename std::enable_if_t<is_int_between<MaximumBits,17,32,true>::value>> { typedef unsigned long int type; };
template <int MaximumBits>
struct _HTBitFieldAccessorType<MaximumBits,typename std::enable_if_t<is_int_between<MaximumBits,33,64,true>::value>> { typedef unsigned long long int type; };

template<typename T,std::size_t SizeBytes,std::size_t BitOffset,std::size_t BitNum>
struct _BitFieldMember {
	// Static
	//////////////////////////////////////////////////////////////////////////
	static_assert(std::is_trivial<T>::value,"T Must be a trivial type!");
	static_assert(BitOffset >=0 && BitOffset+BitNum <= 8*SizeBytes,"Bit range is out of bounds!");

	enum : unsigned char {
		BitOffsetInByte = BitOffset & 0x7u, // % 8
	};

	typedef typename _HTBitFieldAccessorType<BitNum + BitOffsetInByte>::type MemoryAccessType;
	enum : unsigned char {
		TypeBitLength = sizeof(T) << 3u,
	};

	enum : MemoryAccessType {
		MemoryAccessTypeMaximum = static_cast<MemoryAccessType>(~0x0),
		Maximum = static_cast<MemoryAccessType>(MemoryAccessTypeMaximum >> ((sizeof(MemoryAccessType)<<3u) - BitNum)),
	};

	enum : std::size_t {
		TargetByte = BitOffset >> 3u, // / 8
	};

	static_assert(BitNum <= 56u + (8u - BitOffsetInByte),"BitNum is too large, this variable must be aligned within a single QWORD!");

	// Local
	//////////////////////////////////////////////////////////////////////////
	char data[SizeBytes];

	// Helper
	//////////////////////////////////////////////////////////////////////////
	template<typename Q = T>
	inline typename std::enable_if_t<std::is_same<Q,bool>::value,bool> _isValueValid(const Q& v) {
		return (MemoryAccessType(v) & ~Maximum) == 0x0;
	}

	template<typename Q = T>
	inline typename std::enable_if_t<!std::is_same<Q,bool>::value,bool> _isValueValid(const Q& v) {
		return (v < 0 ? (~(v << 1u)) : (v)) <= Maximum;
	}

	// Mutation
	//////////////////////////////////////////////////////////////////////////
private:
	// Do not allow the default assignment operator to be called, as it will overwrite everything in the entire data structure with the data of the left hand operator
	_BitFieldMember& operator=(const _BitFieldMember&) = default;

public:
	inline operator const T() const {
		const MemoryAccessType& targetValue = *reinterpret_cast<const MemoryAccessType*>(data + TargetByte);
		return T((targetValue >> BitOffsetInByte) & Maximum);
	}

	inline const T operator*() const {
		return (T)*this;
	}

	inline _BitFieldMember& operator=(const T& v) {
		assert(_isValueValid(v)); // v must fit inside the bitfield member

		MemoryAccessType& targetValue = *reinterpret_cast<MemoryAccessType*>(data + TargetByte);
		targetValue = (targetValue & ~(Maximum << BitOffsetInByte)) | (v << BitOffsetInByte);
		return *this;
	}

	// Comparison
	//////////////////////////////////////////////////////////////////////////
	template<typename RT>
	inline bool operator==(const RT& r) const {
		return ((T)*this) == r;
	}

	template<typename RT>
	inline bool operator!=(const RT& r) const {
		return ((T)*this) != r;
	}

	template<typename RT>
	inline bool operator>(const RT& r) const {
		return ((T)*this) > r;
	}

	template<typename RT>
	inline bool operator>=(const RT& r) const {
		return ((T)*this) >= r;
	}

	template<typename RT>
	inline bool operator<(const RT& r) const {
		return ((T)*this) < r;
	}

	template<typename RT>
	inline bool operator<=(const RT& r) const {
		return ((T)*this) <= r;
	}
};

template<typename T,std::size_t SizeBytes,std::size_t BitOffset,std::size_t BitNum,std::size_t ArrayBitOffset,std::size_t ArrayElementBitLength,std::size_t ArrayElementCount>
struct _BitFieldArrayMember {
	// Static
	//////////////////////////////////////////////////////////////////////////
	static_assert(BitOffset >=0 && BitOffset+BitNum <= ArrayElementBitLength,"Bit range is out of bounds!");
	static_assert(BitNum <= 56u,"BitNum is too large, this variable must be aligned within a single QWORD!");

	typedef typename _HTBitFieldAccessorType<BitNum+8>::type MemoryAccessType;
	enum : unsigned char {
		TypeBitLength = sizeof(T) << 3u,
	};

	enum : MemoryAccessType {
		MemoryAccessTypeMaximum = static_cast<MemoryAccessType>(~0x0),
		Maximum = static_cast<MemoryAccessType>(MemoryAccessTypeMaximum >> ((sizeof(MemoryAccessType)<<3u) - BitNum)),
	};

	// Local
	//////////////////////////////////////////////////////////////////////////
	char* data;
	std::size_t arrayIndex;

	// Helper
	//////////////////////////////////////////////////////////////////////////
	template<typename Q = T>
	inline typename std::enable_if_t<std::is_same<Q,bool>::value,bool> _isValueValid(const Q& v) {
		return (MemoryAccessType(v) & ~Maximum) == 0x0;
	}

	template<typename Q = T>
	inline typename std::enable_if_t<!std::is_same<Q,bool>::value,bool> _isValueValid(const Q& v) {
		return (v < 0 ? (~(v << 1u)) : (v)) <= Maximum;
	}

	inline std::size_t _getOverallBitOffset() const {
		return ArrayBitOffset + ArrayElementBitLength*arrayIndex + BitOffset;
	}

	// Mutation
	//////////////////////////////////////////////////////////////////////////
private:
	// Do not allow the default assignment operator to be called, as it will overwrite everything in the entire data structure with the data of the left hand operator
	_BitFieldArrayMember& operator=(const _BitFieldArrayMember&) = default;

public:
	inline operator const T() const {
		const std::size_t OverallBitOffset = _getOverallBitOffset();
		const unsigned char BitOffsetInByte = OverallBitOffset & 0x7u;
		const std::size_t TargetByte = OverallBitOffset >> 3u;

		const MemoryAccessType& targetValue = *reinterpret_cast<const MemoryAccessType*>(data + TargetByte);
		return T((targetValue >> BitOffsetInByte) & Maximum);
	}

	inline const T operator*() const {
		return (T)*this;
	}

	inline _BitFieldArrayMember& operator=(const T& v) {
		assert(_isValueValid(v)); // v must fit inside the bitfield member

		const std::size_t OverallBitOffset = _getOverallBitOffset();
		const unsigned char BitOffsetInByte = OverallBitOffset & 0x7u;
		const std::size_t TargetByte = OverallBitOffset >> 3u;

		MemoryAccessType& targetValue = *reinterpret_cast<MemoryAccessType*>(data + TargetByte);
		targetValue = (targetValue & ~(Maximum << BitOffsetInByte)) | (v << BitOffsetInByte);
		return *this;
	}

	// Comparison
	//////////////////////////////////////////////////////////////////////////
	template<typename RT>
	inline bool operator==(const RT& r) const {
		return ((T)*this) == r;
	}

	template<typename RT>
	inline bool operator!=(const RT& r) const {
		return ((T)*this) != r;
	}

	template<typename RT>
	inline bool operator>(const RT& r) const {
		return ((T)*this) > r;
	}

	template<typename RT>
	inline bool operator>=(const RT& r) const {
		return ((T)*this) >= r;
	}

	template<typename RT>
	inline bool operator<(const RT& r) const {
		return ((T)*this) < r;
	}

	template<typename RT>
	inline bool operator<=(const RT& r) const {
		return ((T)*this) <= r;
	}
};

#define HTBitFieldTypeBegin(Name,BufferSizeBytes) \
union Name {\
	typedef _HTBitFieldAccessorType<BufferSizeBytes*8>::type MemoryAccessType;\
	typedef MemoryAccessType DataAccessType;\
	static const bool IsSingleOperationAccess = is_int_between<BufferSizeBytes*8,1,64,true>::value;\
enum : std::size_t {BufferSize = BufferSizeBytes};\
	char data[BufferSize];\
	/*// Constructors, default and (char*,int) //////////////////////////////////////////////////////////////////////////*/\
	inline Name() = default;\
	/*inline Name() { memset(data,0,BufferSize); }*/\
	inline Name(char* value,int valueLen) { memcpy(data,value,valueLen); }\
	inline Name(const Name& other) { memcpy(data,other.data,BufferSize); }\
	/*// Constructor, DataAccessType (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline Name(const DataAccessType& v) {\
		*this = v;\
	}\
	/*// Implicit conversion operator, DataAccessType (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline operator DataAccessType() const {\
		enum : MemoryAccessType {\
			MemoryAccessTypeMaximum = static_cast<MemoryAccessType>(~0x0),\
			Maximum = static_cast<MemoryAccessType>(MemoryAccessTypeMaximum >> ((sizeof(MemoryAccessType)<<3u) - BufferSizeBytes*8)),\
		};\
		return *reinterpret_cast<const MemoryAccessType*>(data) & Maximum;\
	}\
	/*// dereference operator, DataAccessType (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline typename DataAccessType operator*() const {\
		return (DataAccessType)*this;\
	}\
	/*// shift operators, DataAccessType (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline DataAccessType operator<<(const std::size_t r) const {\
		return ((DataAccessType)*this) << r;\
	}\
	/*// shift operators, DataAccessType (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline DataAccessType operator>>(const std::size_t r) const {\
		return ((DataAccessType)*this) >> r;\
	}\
	inline Name& operator=(const Name& other) { memcpy(data,other.data,BufferSize); return *this; }\
	/*// Equality operator, DataAccessType (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline Name& operator=(const typename _DoNotDeduce_t<DataAccessType>& v) {\
		enum : MemoryAccessType {\
			MemoryAccessTypeMaximum = static_cast<MemoryAccessType>(~0x0),\
			Maximum = static_cast<MemoryAccessType>(MemoryAccessTypeMaximum >> ((sizeof(MemoryAccessType)<<3u) - BufferSizeBytes*8)),\
		};\
		MemoryAccessType& targetValue = *reinterpret_cast<MemoryAccessType*>(data);\
		targetValue = (targetValue & ~Maximum) | (v & Maximum);\
		return *this;\
	}\
	/*// Comparison operators, (BufferSizeBytes*8 = [1,64]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator==(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) == r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator!=(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) != r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator>(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) > r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator>=(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) >= r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator<(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) < r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator<=(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) <= r;\
	}\

#define HTBitFieldTypeEnd() \
};

#define HTBitFieldMember(Name,Type,BitOffset,BitLength) \
	_BitFieldMember<Type,BufferSize,BitOffset,BitLength> Name;

#define HTBitFieldArrayBegin(Name,BitOffset,ElementBitLength,ElementCount) \
	static_assert(BitOffset >=0 && BitOffset+(ElementBitLength*ElementCount)<= 8*BufferSize,"Bit range is out of bounds!");\
union T_##Name##_ElementAccessor; \
union __##Name {\
	char data[BufferSize];\
	T_##Name##_ElementAccessor operator[](std::size_t index) {\
		assert(index >= 0 && index < ElementCount);\
		T_##Name##_ElementAccessor ret = {data,index};\
		return ret;\
	}\
	const T_##Name##_ElementAccessor operator[](std::size_t index) const {\
		assert(index >= 0 && index < ElementCount);\
		T_##Name##_ElementAccessor ret = {(char*)data,index};\
		return ret;\
	}\
} Name;\
union T_##Name##_ElementAccessor {\
	typedef _HTBitFieldAccessorType<ElementBitLength+8>::type MemoryAccessType;\
	typedef _HTBitFieldAccessorType<ElementBitLength>::type DataAccessType;\
	static const bool IsSingleOperationAccess = is_int_between<ElementBitLength,1,56,true>::value;\
	enum : std::size_t {\
		ArrayBitOffset = BitOffset,\
		ArrayElementBitLength = ElementBitLength,\
		ArrayElementCount = ElementCount\
	};\
	struct {\
		char* data;\
		std::size_t arrayIndex;\
		} wrapper;\
	/*// Implicit conversion operator, MemoryAccessType (BitsToAccess = [1,56]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline operator DataAccessType() const {\
		enum : MemoryAccessType {\
			MemoryAccessTypeMaximum = static_cast<MemoryAccessType>(~0x0),\
			Maximum = static_cast<MemoryAccessType>(MemoryAccessTypeMaximum >> ((sizeof(MemoryAccessType)<<3u) - ElementBitLength)),\
		};\
		const std::size_t OverallBitOffset = BitOffset + ElementBitLength*wrapper.arrayIndex;\
		const unsigned char BitOffsetInByte = OverallBitOffset & 0x7u;\
		const std::size_t TargetByte = OverallBitOffset >> 3u;\
		return static_cast<DataAccessType>((*reinterpret_cast<const MemoryAccessType*>(wrapper.data + TargetByte) >> BitOffsetInByte) & Maximum);\
	}\
	/*// dereference operator, MemoryAccessType (BitsToAccess = [1,56]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline const DataAccessType operator*() const {\
		return (DataAccessType)*this;\
	}\
	/*// Equality operator, MemoryAccessType (BitsToAccess = [1,56]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline T_##Name##_ElementAccessor& operator=(const typename _DoNotDeduce_t<DataAccessType>& v) {\
		enum : MemoryAccessType {\
			MemoryAccessTypeMaximum = static_cast<MemoryAccessType>(~0x0),\
			Maximum = static_cast<MemoryAccessType>(MemoryAccessTypeMaximum >> ((sizeof(MemoryAccessType)<<3u) - ElementBitLength)),\
		};\
		const std::size_t OverallBitOffset = BitOffset + ElementBitLength*wrapper.arrayIndex;\
		const unsigned char BitOffsetInByte = OverallBitOffset & 0x7u;\
		const std::size_t TargetByte = OverallBitOffset >> 3u;\
		MemoryAccessType& targetValue = *reinterpret_cast<MemoryAccessType*>(wrapper.data + TargetByte);\
		targetValue = (targetValue & ~(Maximum << BitOffsetInByte)) | (v << BitOffsetInByte);\
		return *this;\
	}\
	/*// Equality operator, ElementAccessor //////////////////////////////////////////////////////////////////////////*/\
	inline T_##Name##_ElementAccessor& operator=(const T_##Name##_ElementAccessor& r) {\
		_setTo(r);\
		return *this;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline void _setTo(const T_##Name##_ElementAccessor& r) {\
		const DataAccessType data = r;\
		*this = data;\
	}\
	template<_HTBF_ENABLE_IF_NOT_SINGLE_OPERATION_ACCESS,typename = void>/*Have to add a 'dummy' parameter here, or the compiler will complain that _setTo() is multiply defined*/\
	inline void _setTo(const T_##Name##_ElementAccessor& r) {\
		const std::size_t OverallBitOffsetDest = BitOffset + ElementBitLength*wrapper.arrayIndex;\
		const std::size_t OverallBitOffsetSource = BitOffset + ElementBitLength*r.wrapper.arrayIndex;\
		memcpy_bits(wrapper.data,OverallBitOffsetDest,r.wrapper.data,OverallBitOffsetSource,ElementBitLength);\
	}\
	/*// Comparison operators, (BitsToAccess = [1,56]) //////////////////////////////////////////////////////////////////////////*/\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator==(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) == r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator!=(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) != r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator>(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) > r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator>=(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) >= r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator<(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) < r;\
	}\
	template<_HTBF_ENABLE_IF_SINGLE_OPERATION_ACCESS>\
	inline bool operator<=(const typename _DoNotDeduce_t<DataAccessType>& r) const {\
		return ((MemoryAccessType)*this) <= r;\
	}\

#define HTBitFieldArrayEnd() \
};

#define HTBitFieldArrayMember(Name,Type,BitOffset,BitLength) \
	_BitFieldArrayMember<Type,BufferSize,BitOffset,BitLength,ArrayBitOffset,ArrayElementBitLength,ArrayElementCount> Name;

#pragma warning( pop )