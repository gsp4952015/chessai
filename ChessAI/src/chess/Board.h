#pragma once

#include <functional>
#include <numeric>
#include <algorithm>

#include "Utility.h"
#include "HTBitField.h"

#include "Types.h"

namespace chess {
	class board {
		static const uint8 _GameHashHistorySize = 30;
		struct _TGameHashHistoryEntry {
			uint64 preMoveHash;
			TBoardMove move;
		};

	public:
		TBoardData boardData;
		uint64 transpositionHash;
		uint64 tileAttackers[64]; // a bit board for each tile, shows which ENEMY tiles can attack it
		uint64 tileDefenders[64]; // a bit board for each tile, shows which FRIENDLY tiles can attack it
		TBoardPosition kingLocations[2];
		bool fileIsVulnurableToEnPassant[2][8];
		bool castlingRights[2][2];
		bool hasAlreadyCastled[2];

		bool isColorInCheck[2];
		uint16 colorMobility[2];

		uint8 movePenaltyHistory[2][evaluation_data::RepeatedPieceMoveLogSize];
		uint8 movePenaltyHistoryInsertionIndex;
		_TGameHashHistoryEntry gameHashHistory[_GameHashHistorySize];
		uint8 gameHashHistoryInsertionIndex;
		uint16 currentPly;

		piece_color colorThatCanMove;

	public:
		typedef decltype(TBoardData().tiles[0]) TTileStateModifier;

		inline board() { transpositionHash = 0; resetBoard(); }
		inline board(const board& other) = default;

		inline board& operator=(const board& other) = default;

		void resetBoard();

		void recalculateTranspositionHash();

		inline int32 evaluate(const piece_color sideToEvaluate) const;
		inline int32 see(const TBoardPosition tilePosition,const piece_color color) const;
		inline int32 seeMove(const TBoardMove move,const piece_color color) const;

		inline bool wouldMoveViolateThreefoldRule(const TBoardMove move) const;

		// NOTE: cannot decipher a castling
		inline move_mode determineMostLikelyMoveMode(const TBoardPosition from,const TBoardPosition to) const;

		enum EApplyMoveFlags : uint8 {
			amf_moveTiles = 1 << 0,
			amf_endTurn = 1 << 1,
			amf_calculateDerivedData = 1 << 2,

			amf_all = amf_moveTiles | amf_endTurn | amf_calculateDerivedData
		};
		inline void applyMove(const TBoardMove move,const uint8 flags = amf_all);

		inline void runMoveGenerator(TBoardPossibleMoves* possibleMoves,const piece_color color) const;

		template<typename FUNC>
		inline void visitAllPossibleMoves(const piece_color color,const FUNC visitFunc,const uint16 validModeFlags = MoveModeFlags_AllMakable,const uint64 tilesToCheck = ~uint64(0)) const;

		inline uint64 getTranspositionHash() const { return transpositionHash; }

		inline TTileStateModifier getTileStateModifier(const uint8& x,const uint8& y) { return boardData.tiles[(y<<3) + x]; }
		inline const TTileStateModifier getTileStateModifier(const uint8& x,const uint8& y) const { return boardData.tiles[(y<<3) + x]; }
		// packedData should have x encoded into bits 0..2, and y in bits 3..5
		inline TTileStateModifier getTileStateModifier(const TBoardPosition& packedData) { return boardData.tiles[packedData]; }
		inline const TTileStateModifier getTileStateModifier(const TBoardPosition& packedData) const { return boardData.tiles[packedData]; }
	};

	// Inline methods
	//////////////////////////////////////////////////////////////////////////
	inline chess::int32 chess::board::evaluate(const piece_color sideToEvaluate) const {
		//https://chessprogramming.wikispaces.com/CPW-Engine_eval
		//https://github.com/official-stockfish/Stockfish/blob/master/src/evaluate.cpp
		using namespace evaluation_data;

		struct {
			TBoardPosition pieceLocations[2][num_pieces][9];
			uint8 pieceCount[2][num_pieces];
			uint8 cheapestAttackerTypes[2][num_pieces][9];
			uint8 cheapestDefenderTypes[2][num_pieces][9];
			TBoardPosition kingRing[2][8];
			uint8 kingRing_enemyAttackMobility[2];
			uint8 kingRing_enemyOccupants[2];
			uint8 kingRing_friendlyOccupants[2];
		} evalData = {};

		struct {
			int32 staticMod;
			int32 material;
			int32 positional;
			int32 piece;
			int32 threat;
			int32 mobility;
		} scoreCounters = {};

		// Populate the evalData structure
		//////////////////////////////////////////////////////////////////////////

		// tabulate piece counters
		for(uint8 targetTileIndex = 0; targetTileIndex < 64; targetTileIndex++) {
			const auto targetTile = getTileStateModifier(targetTileIndex);
			const uint8 pieceColor = targetTile.pieceColor;
			const uint8 pieceType = targetTile.pieceType;
			if(pieceType != none) {
				auto& pieceCountRef = evalData.pieceCount[pieceColor][pieceType];
				evalData.pieceLocations[pieceColor][pieceType][pieceCountRef] = targetTileIndex;
				evalData.cheapestAttackerTypes[pieceColor][pieceType][pieceCountRef] = targetTile.getCheapestAttackerType(NextColor[pieceColor]);
				evalData.cheapestDefenderTypes[pieceColor][pieceType][pieceCountRef] = targetTile.getCheapestAttackerType(pieceColor);
				pieceCountRef++;
			}
		}

		// king ring
		for(uint8 color = 0; color < 2; color++) {
			if(!kingLocations[color].isValid()) continue;

			evalData.kingRing[color][0] = kingLocations[color].getWithOffset(-1,1);
			evalData.kingRing[color][1] = kingLocations[color].getWithOffset(0,1);
			evalData.kingRing[color][2] = kingLocations[color].getWithOffset(1,1);
			evalData.kingRing[color][3] = kingLocations[color].getWithOffset(-1,0);
			evalData.kingRing[color][4] = kingLocations[color].getWithOffset(1,0);
			evalData.kingRing[color][5] = kingLocations[color].getWithOffset(-1,-1);
			evalData.kingRing[color][6] = kingLocations[color].getWithOffset(0,-1);
			evalData.kingRing[color][7] = kingLocations[color].getWithOffset(1,-1);

			for(uint8 ringIndex = 0; ringIndex < 8; ringIndex++) {
				if(evalData.kingRing[color][ringIndex].isValid()) {
					const auto kingRingTile = getTileStateModifier(evalData.kingRing[color][ringIndex]);
					evalData.kingRing_enemyAttackMobility[color] += NumberOfSetBits64(tileAttackers[(uint8)evalData.kingRing[color][ringIndex]]);
					if(kingRingTile.pieceType != none) {
						if(kingRingTile.pieceColor == color) {
							evalData.kingRing_friendlyOccupants[color]++;
						} else {
							evalData.kingRing_enemyOccupants[color]++;
						}
					}
				}
			}
		}

		// Calculate score
		//////////////////////////////////////////////////////////////////////////

		// static score
		for(uint8 color = 0; color < 2; color++) {
			int32 colorValue = 0;

			// repeated moves
			; {
				uint8 numberRepeatedMovesInLog = 0;
				int8 numberMovesInLog[num_pieces] = {};
				for(uint8 logIndex = 0; logIndex < evaluation_data::RepeatedPieceMoveLogSize; logIndex++) {
					const auto curType = movePenaltyHistory[color][logIndex];
					if(curType == none) continue;
					numberMovesInLog[curType]++;
				}
				for(auto& logItem : numberMovesInLog) numberRepeatedMovesInLog += std::max<int8>(0,logItem-1);
				colorValue += evaluation_data::RepeatedPieceMovePenality[numberRepeatedMovesInLog];
			}

			// castling rights
			for(uint8 side = 0; side < 2; side++)
				colorValue += castlingRights[color][side] * evaluation_data::HasCastlingRightsBonus[side];

			// has already castled
			colorValue += hasAlreadyCastled[color] * evaluation_data::HasAlreadyCastledBonus;

			scoreCounters.staticMod += ColorMultiplier[color] * int32((float)colorValue * evaluation_data::AIPersonalities[color].weights.staticMod);
		}

		// material score
		for(uint8 color = 0; color < 2; color++) {
			int32 colorValue = 0;

			for(uint8 pieceType = pawn; pieceType < num_pieces; pieceType++) {
				colorValue += MaterialValues[pieceType] * evalData.pieceCount[color][pieceType];
			}

			// material adjustments
			colorValue += evalData.pieceCount[color][knight] * MaterialAdj_Knight_NumPawns[evalData.pieceCount[color][pawn]];
			colorValue += evalData.pieceCount[color][rook] * MaterialAdj_Rook_NumPawns[evalData.pieceCount[color][pawn]];

			scoreCounters.material += ColorMultiplier[color] * int32((float)colorValue * evaluation_data::AIPersonalities[color].weights.material);
		}

		// piece score
		// TODO: add per-piece logic
		for(uint8 color = 0; color < 2; color++) {
			int32 colorValue = 0;

			for(uint8 pieceType = pawn; pieceType < num_pieces; pieceType++) {
				if(evalData.pieceCount[color][pieceType] == PieceTotalCount[pieceType])
					colorValue += AllPiecesBonus[pieceType];
			}

			// king ring
			colorValue += evalData.kingRing_enemyAttackMobility[color] * evaluation_data::KingRingAttackMobilityMultiplier;
			colorValue += evaluation_data::KingRingFriendlyOccupantsBonus[evalData.kingRing_friendlyOccupants[color]];
			colorValue += evaluation_data::KingRingEnemyOccupantsBonus[evalData.kingRing_enemyOccupants[color]];

			scoreCounters.piece += ColorMultiplier[color] * int32((float)colorValue * evaluation_data::AIPersonalities[color].weights.piece);
		}

		// positional and threat score
		for(uint8 color = 0; color < 2; color++) {
			int32 colorValue_positional = 0;
			int32 colorValue_threat = 0;

			for(uint8 pieceType = pawn; pieceType < num_pieces; pieceType++) {
				for(uint8 pieceInd = 0; pieceInd < evalData.pieceCount[color][pieceType]; pieceInd++) {
					const TBoardPosition& boardPosition = evalData.pieceLocations[color][pieceType][pieceInd];
					const uint8 boardIndex = boardPosition;

					if(pieceType == pawn) {
						const uint8 pawnViewRank = color == white ? boardPosition.y : 7 - boardPosition.y;
						colorValue_positional += evaluation_data::PawnRankBonus[pawnViewRank];
						if(pawnViewRank == 6) {
							const TBoardPosition posPromotePosition = boardPosition.getWithOffset(0,ColorMultiplier[color]);
							const auto tilePromotePosition = getTileStateModifier(posPromotePosition);

							int numattackers = NumberOfSetBits64(tileAttackers[(uint8)posPromotePosition]);
							if(tilePromotePosition.getCheapestAttackerType(NextColor[color]) == none) {
								colorValue_positional += evaluation_data::PawnCanPromoteBonus;
							}
						}
					}

					const uint8 tableIndex = color==black ? boardIndex : 63-boardIndex;
					colorValue_positional += PositionValueTables[pieceType][tableIndex];

					const uint8 targetQuality = PieceQualityMap[pieceType];
					const uint8 attackerQuality = PieceQualityMap[evalData.cheapestAttackerTypes[color][pieceType][pieceInd]];
					const uint8 defenderQuality = PieceQualityMap[evalData.cheapestDefenderTypes[color][pieceType][pieceInd]];
					colorValue_threat += evaluation_data::ThreatPenalty[attackerQuality][targetQuality][defenderQuality!=pq_none];
					colorValue_threat += evaluation_data::DefenseBonus[defenderQuality][targetQuality][attackerQuality!=pq_none];
				}
			}

			scoreCounters.positional += ColorMultiplier[color] * int32((float)colorValue_positional * evaluation_data::AIPersonalities[color].weights.positional);
			scoreCounters.threat += ColorMultiplier[color] * int32((float)colorValue_threat * evaluation_data::AIPersonalities[color].weights.threat);
		}

		// mobility score
		for(uint8 color = 0; color < 2; color++) {
			int32 colorValue = 0;
			colorValue += colorMobility[color];
			scoreCounters.mobility += ColorMultiplier[color] * int32((float)colorValue * evaluation_data::AIPersonalities[color].weights.mobility);
		}

		return ColorMultiplier[sideToEvaluate] * (scoreCounters.staticMod +
												  scoreCounters.material +
												  scoreCounters.positional +
												  scoreCounters.threat +
												  scoreCounters.piece +
												  scoreCounters.mobility);
	}

	inline int32 chess::board::see(const TBoardPosition tilePosition,const piece_color color) const {
		int32 value = 0;
		// get the lowest value piece that can attack tilePosition
		TBoardPosition smallestAttackerPosition = InvalidPosition;
		; {
			const uint64 tilesToCheck = tileAttackers[(uint8)tilePosition];
			uint16 smallestValue = -1;
			for(uint8 checkPosition = 0; checkPosition < 64; checkPosition++) {
				if(!(tilesToCheck & (1ui64 << checkPosition))) continue;
				const auto tileToCheck = getTileStateModifier(checkPosition);
				const uint16 checkValue = evaluation_data::MaterialValues[tileToCheck.pieceType];
				if(checkValue > 0 && checkValue < smallestValue) {
					smallestValue = checkValue;
					smallestAttackerPosition = checkPosition;
				}
			}
		}

		if(smallestAttackerPosition.isValid()) {
#ifdef _DEBUG
			// debug
			const auto tsm_from = getTileStateModifier(smallestAttackerPosition);
			const auto tsm_to = getTileStateModifier(tilePosition);

			const volatile uint8 fromX = smallestAttackerPosition.x;
			const volatile uint8 fromY = smallestAttackerPosition.y;
			const volatile piece_type fromType = static_cast<piece_type>((uint8)tsm_from.pieceType);
			const volatile piece_color fromColor = static_cast<piece_color>((uint8)tsm_from.pieceColor);
			const volatile uint8 toX = tilePosition.x;
			const volatile uint8 toY = tilePosition.y;
			const volatile piece_type toType = static_cast<piece_type>((uint8)tsm_to.pieceType);
			const volatile piece_color toColor = static_cast<piece_color>((uint8)tsm_to.pieceColor);

			assert(tsm_from.pieceType != none);
			assert(tsm_from.pieceColor == color);
			assert(tsm_to.pieceType != none);
			assert(tsm_to.pieceColor == NextColor[color]);
#endif

			const move_mode tmpMoveMode = determineMostLikelyMoveMode(smallestAttackerPosition,tilePosition);

			// promotion logic
			switch(tmpMoveMode) {
				case mm_promotion_queen:
				case mm_promotion_queen_capture:
					value = evaluation_data::MaterialValues[queen] - evaluation_data::MaterialValues[pawn];
					break;
				case mm_promotion_knight:
				case mm_promotion_knight_capture:
					value = evaluation_data::MaterialValues[queen] - evaluation_data::MaterialValues[pawn];
					break;
			}

			const int32 pieceJustCapturedValue = evaluation_data::MaterialValues[getTileStateModifier(tilePosition).pieceType];
			board tempBoard = *this;
			tempBoard.applyMove(TBoardMove(smallestAttackerPosition,tilePosition,tmpMoveMode),amf_moveTiles|amf_calculateDerivedData);
			value = std::max<int32>(0,value + pieceJustCapturedValue - tempBoard.see(tilePosition,NextColor[color]));
		}

		return value;
	}

	inline int32 chess::board::seeMove(const TBoardMove move,const piece_color color) const {
		int32 value = 0;

		// debug
#ifdef _DEBUG
		assert(move.isValid());

		const auto tsm_from = getTileStateModifier(move.getFromPosition());
		const auto tsm_to = getTileStateModifier(move.getToPosition());

		const volatile uint8 fromX = move.getFromPosition().x;
		const volatile uint8 fromY = move.getFromPosition().y;
		const volatile piece_type fromType = static_cast<piece_type>((uint8)tsm_from.pieceType);
		const volatile piece_color fromColor = static_cast<piece_color>((uint8)tsm_from.pieceColor);
		const volatile uint8 toX = move.getToPosition().x;
		const volatile uint8 toY = move.getToPosition().y;
		const volatile piece_type toType = static_cast<piece_type>((uint8)tsm_to.pieceType);
		const volatile piece_color toColor = static_cast<piece_color>((uint8)tsm_to.pieceColor);

		assert(tsm_from.pieceType != none);
		assert(tsm_from.pieceColor == color);
		assert(!move.isACapture() || tsm_to.pieceType != none || (tsm_to.pieceType == none && move.mode == mm_capture_en_passant));
		assert(!move.isACapture() || tsm_to.pieceColor == NextColor[color] || move.mode == mm_capture_en_passant);
#endif

		const TBoardPosition toPosition = move.getToPosition();

		// promotion logic
		switch(move.mode) {
			case mm_promotion_queen:
			case mm_promotion_queen_capture:
				value = evaluation_data::MaterialValues[queen] - evaluation_data::MaterialValues[pawn];
				break;
			case mm_promotion_knight:
			case mm_promotion_knight_capture:
				value = evaluation_data::MaterialValues[queen] - evaluation_data::MaterialValues[pawn];
				break;
		}

		if(move.isACapture()) {
			const int32 pieceJustCapturedValue = evaluation_data::MaterialValues[getTileStateModifier(toPosition).pieceType];
			board tempBoard = *this;
			tempBoard.applyMove(move,amf_moveTiles|amf_calculateDerivedData);
			value = value + pieceJustCapturedValue - tempBoard.see(toPosition,NextColor[color]);
		} else {
			board tempBoard = *this;
			tempBoard.applyMove(move,amf_moveTiles|amf_calculateDerivedData);
			value = value + tempBoard.see(toPosition,NextColor[color]);
		}

		return value;
	}

	inline bool chess::board::wouldMoveViolateThreefoldRule(const TBoardMove move) const {
		uint8 timesRepeated = 0;
		for(uint8 historyIndex = 0; historyIndex < _GameHashHistorySize; historyIndex++) {
			if(transpositionHash == gameHashHistory[historyIndex].preMoveHash &&
			   move == gameHashHistory[historyIndex].move) {
				timesRepeated++;
			}
		}

		if(timesRepeated >= 2) {
			return true; // this move would violate the rule, it is invalid
		}

		return false;
	}

	// NOTE: cannot decipher a castling
	inline move_mode chess::board::determineMostLikelyMoveMode(const TBoardPosition from,const TBoardPosition to) const {
		const auto tsm_from = getTileStateModifier(from);
		const auto tsm_to = getTileStateModifier(to);

		const piece_type fromType = static_cast<piece_type>((uint8)tsm_from.pieceType);
		const piece_color fromColor = static_cast<piece_color>((uint8)tsm_from.pieceColor);
		const piece_type toType = static_cast<piece_type>((uint8)tsm_to.pieceType);
		const piece_color toColor = static_cast<piece_color>((uint8)tsm_to.pieceColor);

		if(fromType == none) return mm_invalid;
		if(fromColor == toColor) return mm_defense;

		if(fromType != pawn) {
			if(toType == none) return mm_reposition;
			return mm_capture;
		} else {
			if(toType == none) {
				if(from.x == to.x) {
					if(to.y == 7 || to.y == 0) return mm_promotion_queen;
					if(abs((int8)from.y-(int8)to.y) == 2) return mm_reposition_double_pawn_push;
					return mm_reposition;
				}
				if(fileIsVulnurableToEnPassant[NextColor[fromColor]][to.x] &&
				   from.y == en_passant::EnPassantEngaugementRank[fromColor]) {
					return mm_capture_en_passant;
				}
			} else {
				if(from.x != to.x) {
					if(to.y == 7 || to.y == 0) return mm_promotion_queen_capture;
					return mm_capture;
				}
				assert(false);
			}
		}

		assert(false);
		return mm_invalid;
	}

	inline void chess::board::applyMove(const TBoardMove move,const uint8 flags) {
		const uint16 moveData = move;
		const TBoardPosition fromPosition = move.getFromPosition();
		const TBoardPosition toPosition = move.getToPosition();

		int from_x = fromPosition.x;
		int from_y = fromPosition.y;
		int to_x = toPosition.x;
		int to_y = toPosition.y;

		auto fromTile = getTileStateModifier(fromPosition);
		auto toTile = getTileStateModifier(toPosition);

		const piece_type fromType = static_cast<piece_type>((uint8)fromTile.pieceType);
		const piece_color fromColor = static_cast<piece_color>((uint8)fromTile.pieceColor);
		const bool fromHadMoved = fromTile.hasMovedAtAll;
		const piece_type toType = static_cast<piece_type>((uint8)toTile.pieceType);
		const piece_color toColor = static_cast<piece_color>((uint8)toTile.pieceColor);

		uint8 fileMadeVulnurableToEnPassant = -1;

		if(flags & amf_moveTiles) {
			assert(move.isValid());
			assert(fromType != none);
			assert(fromColor == colorThatCanMove || !(flags & amf_endTurn));

			// update history logs
			if(flags & amf_calculateDerivedData) {
				auto& hashHistoryEntry = gameHashHistory[gameHashHistoryInsertionIndex++ % _GameHashHistorySize];
				hashHistoryEntry.preMoveHash = transpositionHash;
				hashHistoryEntry.move = move;

				// don't want to penalize interesting moves
				if(fromType != pawn &&
				   !move.isACapture() &&
				   !move.isAPromotion() &&
				   !move.isACastle()) {
					auto& moveHistoryEntry = movePenaltyHistory[fromColor][movePenaltyHistoryInsertionIndex++ % evaluation_data::RepeatedPieceMoveLogSize];
					moveHistoryEntry = fromType;
				}
			}

			// universal logic
			//////////////////////////////////////////////////////////////////////////

			// update the hash value

			// remove the moving piece from its old position
			transpositionHash ^= hashing::_Hashes_TileState[fromColor][fromType][(uint8)fromPosition];
			// remove the captured piece from its position
			if(toType != none) transpositionHash ^= hashing::_Hashes_TileState[toColor][toType][(uint8)toPosition];
			// add the moving piece to its new position
			transpositionHash ^= hashing::_Hashes_TileState[fromColor][fromType][(uint8)toPosition];

			// apply tile movement
			toTile = fromTile;

			fromTile.pieceType = none;
			toTile.hasMovedAtAll = true;

			// specialized logic
			//////////////////////////////////////////////////////////////////////////

			// PIECE LOGIC
			// handle en passant and castling rights
			switch(fromType) {
				case king: {
					for(uint8 type = 0; type < 2; type++) {
						if(castlingRights[fromColor][type]) {
							transpositionHash ^= hashing::_Hashes_CastlingRights[fromColor][type];
							castlingRights[fromColor][type] = false;
						}
					}
				}
				case rook: {
					if(!fromHadMoved) {
						if(fromPosition.x == castling::ValidCastlingRookLocationFrom_kingside[fromColor]) {
							if(castlingRights[fromColor][castling::CastlingSide::king]) {
								transpositionHash ^= hashing::_Hashes_CastlingRights[fromColor][castling::CastlingSide::king];
								castlingRights[fromColor][castling::CastlingSide::king] = false;
							}
						} else if(fromPosition.x == castling::ValidCastlingRookLocationFrom_queenside[fromColor]) {
							if(castlingRights[fromColor][castling::CastlingSide::queen]) {
								transpositionHash ^= hashing::_Hashes_CastlingRights[fromColor][castling::CastlingSide::queen];
								castlingRights[fromColor][castling::CastlingSide::queen] = false;
							}
						}
					}
				}
			}

			if(move.isACastle()) {
				hasAlreadyCastled[fromColor] = true;
			}

			// MODE LOGIC
			switch(move.mode) {
				case mm_castle_queenside: {
					// invoke another move call to move the king to its new position
					const TBoardMove completeCastlingMove(castling::ValidCastlingKingLocationFrom[fromColor],
														  castling::ValidCastlingKingLocationTo_queenside[fromColor],
														  mm_reposition);
					applyMove(completeCastlingMove,amf_moveTiles);
					break;
				}
				case mm_castle_kingside: {
					// invoke another move call to move the king to its new position
					const TBoardMove completeCastlingMove(castling::ValidCastlingKingLocationFrom[fromColor],
														  castling::ValidCastlingKingLocationTo_kingside[fromColor],
														  mm_reposition);
					applyMove(completeCastlingMove,amf_moveTiles);
					break;
				}
				case mm_reposition_double_pawn_push: {
					const uint8 pawnFile = fromPosition.x;
					fileMadeVulnurableToEnPassant = pawnFile;
					assert(!fileIsVulnurableToEnPassant[fromColor][pawnFile]);
					fileIsVulnurableToEnPassant[fromColor][pawnFile] = true;
					transpositionHash ^= hashing::_Hashes_EnPassantVulnurability[fromColor][pawnFile];
					break;
				}
				case mm_capture_en_passant: {
					const TBoardPosition passerPosition = toPosition.getWithOffset(0,ColorMultiplier[NextColor[fromColor]]);
					auto passerTile = getTileStateModifier(passerPosition);

					assert(passerTile.pieceType == pawn && passerTile.pieceColor != fromColor);

					// remove passer
					transpositionHash ^= hashing::_Hashes_TileState[NextColor[fromColor]][pawn][(uint8)passerPosition];
					passerTile.pieceType = none;

					break;
				}
				case mm_promotion_knight:
				case mm_promotion_knight_capture: {
					// remove the promoted pawn
					transpositionHash ^= hashing::_Hashes_TileState[fromColor][fromType][(uint8)toPosition];
					toTile.pieceType = knight;
					// add the new knight
					transpositionHash ^= hashing::_Hashes_TileState[fromColor][knight][(uint8)toPosition];
					break;
				}
				case mm_promotion_queen:
				case mm_promotion_queen_capture: {
					// remove the promoted pawn
					transpositionHash ^= hashing::_Hashes_TileState[fromColor][fromType][(uint8)toPosition];
					toTile.pieceType = queen;
					// add the new queen
					transpositionHash ^= hashing::_Hashes_TileState[fromColor][queen][(uint8)toPosition];
					break;
				}
			}
		}

		if(flags & amf_endTurn) {
			// swap colors
			transpositionHash ^= hashing::_Hashes_Special[hashing::thst_blacks_turn];
			colorThatCanMove = NextColor[colorThatCanMove];
			assert(currentPly < 0xffffu); // no overflow, game shouldn't ever go on this long
			currentPly++;
		}

		if(flags & amf_calculateDerivedData) {
			// update in-check status and mobility counters
			auto lam_colorPostMove = [&](const piece_color colorFriendly)->void {
				const piece_color colorEnemy = NextColor[colorFriendly];

				isColorInCheck[colorFriendly] = false;
				colorMobility[colorEnemy] = 0;
				visitAllPossibleMoves(colorEnemy,[&](const TBoardMove potentialMove)->void {
					const TBoardPosition fromPosition = potentialMove.getFromPosition();
					const TBoardPosition toPosition = potentialMove.getToPosition();
					const auto fromTile = this->getTileStateModifier(fromPosition);
					auto toTile = this->getTileStateModifier(toPosition);

					switch(potentialMove.mode) {
						case mm_pawn_can_capture:
							colorMobility[colorEnemy]--; // for readability
						case mm_reposition:
						case mm_reposition_double_pawn_push:
						case mm_capture:
						case mm_promotion_queen_capture:
						case mm_promotion_knight_capture: {
							tileAttackers[(uint8)toPosition] |= (1ui64 << (uint8)fromPosition);

							toTile.addAttacker(colorEnemy,fromTile.pieceType);
							if(toTile.pieceColor == colorFriendly && toTile.pieceType == king) {
								isColorInCheck[colorFriendly] = true;
							}
						}
						case mm_capture_en_passant:
						case mm_castle_kingside:
						case mm_castle_queenside:
							colorMobility[colorEnemy]++;
							break;
						case mm_defense: {
							tileDefenders[(uint8)toPosition] |= (1ui64 << (uint8)fromPosition);

							toTile.addAttacker(colorEnemy,fromTile.pieceType);
							break;
						}
					}

					// special logic
					if(potentialMove.mode == mm_capture_en_passant) {
						// must add the attacking pawn to the tileThreats tally of the passing pawn
						const TBoardPosition passerPosition = toPosition.getWithOffset(0,ColorMultiplier[NextColor[colorEnemy]]);
						auto passerTile = this->getTileStateModifier(passerPosition);
						tileAttackers[(uint8)passerPosition] |= (1ui64 << (uint8)fromPosition);
						//passerTile.addAttacker(colorEnemy,fromTile.pieceType);
					}
				},MoveModeFlags_All);
			};

			// clear tile flags
			; {
				const TBoardPosition moveToPosition = move.getToPosition();

				for(uint8 file = 0; file < 8; file++) {
					const bool isSamePawnThatJustMoved = fileMadeVulnurableToEnPassant != -1 && file == fileMadeVulnurableToEnPassant;
					if(fileIsVulnurableToEnPassant[fromColor][file] && !isSamePawnThatJustMoved) {
						fileIsVulnurableToEnPassant[fromColor][file] = false;
						transpositionHash ^= hashing::_Hashes_EnPassantVulnurability[fromColor][file];
					}
					const bool justCapturedThisPawn = move.isACapture() && moveToPosition.x == file && moveToPosition.y == en_passant::EnPassantVulnurabilityRank_FromTo[NextColor[fromColor]][1];
					if(fileIsVulnurableToEnPassant[toColor][file] && justCapturedThisPawn) {
						fileIsVulnurableToEnPassant[toColor][file] = false;
						transpositionHash ^= hashing::_Hashes_EnPassantVulnurability[toColor][file];
					}
				}
			}

			kingLocations[white] = kingLocations[black] = InvalidPosition;
			for(uint8 targetTileIndex = 0; targetTileIndex < 64; targetTileIndex++) {
				auto targetTile = getTileStateModifier(targetTileIndex);

				if(targetTile.pieceType == king) kingLocations[targetTile.pieceColor] = targetTileIndex;

				tileAttackers[targetTileIndex] = 0;
				tileDefenders[targetTileIndex] = 0;

				targetTile.setCheapestAttackerType(white,none);
				targetTile.setCheapestAttackerType(black,none);
			}

			// recalculate tile data
			lam_colorPostMove(white);
			lam_colorPostMove(black);
		}

#ifdef _DEBUG
		if(flags & amf_calculateDerivedData) {
			const uint64 oldHash = transpositionHash;
			recalculateTranspositionHash();
			assert(oldHash == 0 || oldHash == transpositionHash);
		}
#endif
	}

	inline void chess::board::runMoveGenerator(TBoardPossibleMoves* possibleMoves,const piece_color color) const {
		// adds the given move, unless we are in check and this move does not get us out
		auto lam_tryAddMove = [&](const TBoardMove moveData)->void {
			// ensure this move doesn't violate the threefold repetition rule
			if(wouldMoveViolateThreefoldRule(moveData)) return;

			// ensure that this move will not leave us in check
			bool willMoveLeaveUsInCheck = false;

			auto fromTile = getTileStateModifier(moveData.getFromPosition());

			// if it is our king that is moving, we can skip all of the extra move generation and just
			// check to see if any of the entries in the tileCheck structure are enemies
			if(fromTile.pieceType == king) {
				const uint64& threatTiles = tileAttackers[(uint8)moveData.getToPosition()];
				for(uint8 testTileIndex = 0; testTileIndex < 64; testTileIndex++) {
					const TBoardPosition targetTilePosition = testTileIndex;

					const auto targetTile = getTileStateModifier(targetTilePosition);

					if(targetTile.pieceType == none ||
					   targetTile.pieceColor == color ||
					   !(threatTiles & (1ui64 << testTileIndex))) continue;

					return; // this move would leave us in check, ignore it
				}
			}

			auto toTile = getTileStateModifier(moveData.getToPosition());
			const TTileStateModifier::DataAccessType fromDataBackup = fromTile;
			const TTileStateModifier::DataAccessType toDataBackup = toTile;
			toTile = fromTile;
			toTile.hasMovedAtAll = true;
			fromTile.pieceType = none;

			// test for check
			// only test for 'capture' moves, and only from tiles that have
			// previously been determined to be threatening either the to or the from tile, as well as the location of the king
			static const uint16 TestCheckMoveModes = (1 << mm_reposition) | (1 << mm_capture) | (1 << mm_promotion_queen_capture) | (1 << mm_promotion_knight_capture) | (1 << mm_pawn_can_capture);
			const uint64 tilesToCheck =
				tileAttackers[(uint8)moveData.getFromPosition()] |
				(kingLocations[color].isValid() ? tileAttackers[(uint8)kingLocations[color]] : 0);
			visitAllPossibleMoves(NextColor[color],[&](const TBoardMove potentialMove)->void {
				auto potentialToTile = this->getTileStateModifier(potentialMove.getToPosition());
				if(potentialToTile.pieceColor == color &&
				   potentialToTile.pieceType == king) {
					willMoveLeaveUsInCheck = true;
				}
			},TestCheckMoveModes,tilesToCheck);

			fromTile = fromDataBackup;
			toTile = toDataBackup;

			if(willMoveLeaveUsInCheck) return; // this move will us in check, ignore it

			const uint16 currentNumMoves = possibleMoves->numMoves;
			possibleMoves->numMoves = currentNumMoves + 1;
			possibleMoves->moves[currentNumMoves] = (TBoardMove::DataAccessType)moveData;
		};

		possibleMoves->numMoves = 0;

		if(kingLocations[white].isValid() && kingLocations[black].isValid()) {
			visitAllPossibleMoves(color,lam_tryAddMove,MoveModeFlags_AllMakable);
		}
	}

	template<typename FUNC>
	inline void chess::board::visitAllPossibleMoves(const piece_color color,const FUNC visitFunc,const uint16 validModeFlags,const uint64 tilesToCheck) const {
		if(validModeFlags == 0) return;
		if(!kingLocations[color].isValid()) return; // no valid moves if we have no king

		// returns true if a move was generated
		auto lam_standardMoveTest = [&](const TBoardPosition fromPosition,const TBoardPosition toPosition,const uint16 validModeFlags)->move_mode {
			assert(fromPosition.isValid());
			assert(toPosition.isValid());

			const auto toTile = getTileStateModifier(toPosition);

			move_mode desiredMoveType = mm_invalid; // invalid

			if(toTile.isClear()) {
				desiredMoveType = mm_reposition;
			} else if(toTile.hasEnemy(color)) {
				desiredMoveType = mm_capture;
			} else if(toTile.hasAlly(color)) {
				desiredMoveType = mm_defense;
			}

			if(desiredMoveType != mm_invalid && (validModeFlags & (1 << desiredMoveType))) {
				visitFunc(TBoardMove(fromPosition,toPosition,desiredMoveType));
			}

			return desiredMoveType;
		};

		auto lam_rayMoveTest = [&](const TBoardPosition startingPosition,const int8 perStepX,const int8 perStepY,const uint8 numSteps,const uint16 validModeFlags)->void {
			if(validModeFlags == 0) return;

			TBoardPosition curPosition = startingPosition.getWithOffset(perStepX,perStepY);
			for(uint8 step = 0; step < numSteps; step++) {
				if(!curPosition.isValid()) break; // stay within board bounds

				if(lam_standardMoveTest(startingPosition,curPosition,validModeFlags) != mm_reposition) break;

				curPosition = curPosition.getWithOffset(perStepX,perStepY);
			}
		};

		const uint16 pieceValidMoveFlags_general = validModeFlags & ((1 << mm_reposition) | (1 << mm_capture) | (1 << mm_defense));

		for(uint8 targetTileIndex = 0; targetTileIndex < 64; targetTileIndex++) {
			const TBoardPosition targetTilePosition = targetTileIndex;

			const auto targetTile = getTileStateModifier(targetTilePosition);

			if(targetTile.pieceType == none ||
			   targetTile.pieceColor != color ||
			   !(tilesToCheck & (1ui64 << targetTileIndex))) continue;

			switch(targetTile.pieceType) {
				//////////////////////////////////////////////////////////////////////////
				case pawn: {
					const uint16 pawnValidMoveFlags_forward = validModeFlags & (1 << mm_reposition);
					const uint16 pawnValidMoveFlags_diag = validModeFlags & ((1 << mm_capture) | (1 << mm_defense));

					const uint8 toRank = targetTilePosition.y + ColorMultiplier[color];
					const bool isMoveAPromotion = toRank == 0 || toRank == 7;

					; {
						const TBoardPosition singlePushPosition = targetTilePosition.getWithOffset(0,ColorMultiplier[color]);
						const auto singlePushTile = getTileStateModifier(singlePushPosition);

						move_mode generatedMoveType = mm_invalid; // invalid

						if(singlePushTile.isClear()) {
							// mm_reposition
							if(isMoveAPromotion) {
								if(validModeFlags & (1 << mm_promotion_queen)) visitFunc(TBoardMove(targetTilePosition,singlePushPosition,mm_promotion_queen));
								if((validModeFlags & (1 << mm_promotion_knight)) && IsUnderpromotionEnabled) visitFunc(TBoardMove(targetTilePosition,singlePushPosition,mm_promotion_knight));
							} else {
								if(validModeFlags & (1 << mm_reposition)) visitFunc(TBoardMove(targetTilePosition,singlePushPosition,mm_reposition));
							}

							// mm_reposition_double_pawn_push
							if((validModeFlags & (1 << mm_reposition_double_pawn_push)) && !targetTile.hasMovedAtAll) {
								const TBoardPosition doublePushPosition = targetTilePosition.getWithOffset(0,ColorMultiplier[color]*2);
								if(doublePushPosition.isValid()) {
									const auto doublePushTile = getTileStateModifier(doublePushPosition);

									if((validModeFlags & (1 << mm_reposition_double_pawn_push)) && doublePushTile.isClear()) {
										visitFunc(TBoardMove(targetTilePosition,doublePushPosition,mm_reposition_double_pawn_push));
									}
								}
							}
						}
					}

					// pawn capture
					static const int8 xOffsets[] = {1,-1};
					for(uint8 i = 0; i < 2; i++) {
						const TBoardPosition testPositionTo = targetTilePosition.getWithOffset(xOffsets[i],ColorMultiplier[color]);

						if(!testPositionTo.isValid()) continue;

						const auto testTileTo = getTileStateModifier(testPositionTo);

						if(testTileTo.hasEnemy(color)) { // normal pawn capture
							if(isMoveAPromotion) {
								if(validModeFlags & (1 << mm_promotion_queen_capture)) visitFunc(TBoardMove(targetTilePosition,testPositionTo,mm_promotion_queen_capture));
								if((validModeFlags & (1 << mm_promotion_knight_capture)) && IsUnderpromotionEnabled) visitFunc(TBoardMove(targetTilePosition,testPositionTo,mm_promotion_knight_capture));
							} else {
								if(validModeFlags & (1 << mm_capture)) visitFunc(TBoardMove(targetTilePosition,testPositionTo,mm_capture));
							}
						} else if((validModeFlags & (1 << mm_capture_en_passant)) &&
								  testTileTo.isClear() &&
								  fileIsVulnurableToEnPassant[NextColor[color]][testPositionTo.x] &&
								  targetTilePosition.y == en_passant::EnPassantEngaugementRank[color]) { // mm_capture_en_passant
#ifdef _DEBUG
							const TBoardPosition _passerPosition = testPositionTo.getWithOffset(0,-ColorMultiplier[color]);
							const auto _passerTile = getTileStateModifier(_passerPosition);
							const uint8 _from_x = targetTilePosition.x;
							const uint8 _from_y = targetTilePosition.y;
							const uint8 _to_x = testPositionTo.x;
							const uint8 _to_y = testPositionTo.y;
							const uint8 _passer_x = _passerPosition.x;
							const uint8 _passer_y = _passerPosition.y;
							const piece_type _passerTile_type = static_cast<piece_type>((uint8)_passerTile.pieceType);
							const piece_color _passerTile_color = static_cast<piece_color>((uint8)_passerTile.pieceColor);
							assert(_passerTile.pieceType == pawn);
							assert(_passerTile.pieceColor == NextColor[color]);
#endif
							visitFunc(TBoardMove(targetTileIndex,testPositionTo,mm_capture_en_passant));
						} else if((validModeFlags & (1 << mm_defense)) && testTileTo.hasAlly(color)) { // mm_defense
							visitFunc(TBoardMove(targetTilePosition,testPositionTo,mm_defense));
						} else if((validModeFlags & (1 << mm_pawn_can_capture)) && testTileTo.isClear()) { // mm_pawn_can_capture
							visitFunc(TBoardMove(targetTilePosition,testPositionTo,mm_pawn_can_capture));
						}
					}

					break;
				}
						   //////////////////////////////////////////////////////////////////////////
				case rook: {
					lam_rayMoveTest(targetTileIndex,1,0,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,0,1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,-1,0,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,0,-1,7,pieceValidMoveFlags_general);

					// castling
					//////////////////////////////////////////////////////////////////////////

					if(!isColorInCheck[color] && kingLocations[color] == castling::ValidCastlingKingLocationFrom[color]) {
						// king side
						if((validModeFlags & (1 << mm_castle_kingside)) &&
						   castlingRights[color][castling::CastlingSide::king] &&
						   targetTileIndex == castling::ValidCastlingRookLocationFrom_kingside[color]) {
							// ensure that the path is clear between the rook and the king
							bool castlingIsValid = true;
							for(int8 tileOffset = -1; tileOffset > -3; tileOffset--) {
								const auto testTile = getTileStateModifier(targetTileIndex + tileOffset);
								if(!testTile.isClear() ||
								   (tileOffset == -2 && testTile.getCheapestAttackerType(NextColor[color])!=none)) { // cannot move king through an attacked tile
									castlingIsValid = false;
									break;
								}
							}

							if(castlingIsValid) {
								visitFunc(TBoardMove(castling::ValidCastlingRookLocationFrom_kingside[color],castling::ValidCastlingRookLocationTo_kingside[color],mm_castle_kingside));
							}
						}

						// queen side
						if((validModeFlags & (1 << mm_castle_queenside)) &&
						   castlingRights[color][castling::CastlingSide::queen] &&
						   targetTileIndex == castling::ValidCastlingRookLocationFrom_queenside[color]) {
							// ensure that the path is clear between the rook and the king
							bool castlingIsValid = true;
							for(int8 tileOffset = 1; tileOffset < 4; tileOffset++) {
								const auto testTile = getTileStateModifier(targetTileIndex + tileOffset);
								if(!testTile.isClear() ||
								   (tileOffset == 3 && testTile.getCheapestAttackerType(NextColor[color])!=none)) { // cannot move king through an attacked tile
									castlingIsValid = false;
									break;
								}
							}

							if(castlingIsValid) {
								visitFunc(TBoardMove(castling::ValidCastlingRookLocationFrom_queenside[color],castling::ValidCastlingRookLocationTo_queenside[color],mm_castle_queenside));
							}
						}
					}

					break;
				}
						   //////////////////////////////////////////////////////////////////////////
				case knight: {
					static const int8 pieceMoveOffsets[8][2] = {
						{2,1},
						{1,2},
						{2,-1},
						{1,-2},
						{-2,1},
						{-1,2},
						{-2,-1},
						{-1,-2},
					};

					for(uint8 testMoveIndex = 0; testMoveIndex < 8; testMoveIndex++) {
						const TBoardPosition testPosition = targetTilePosition.getWithOffset(pieceMoveOffsets[testMoveIndex][0],pieceMoveOffsets[testMoveIndex][1]);

						if(testPosition.isValid()) {
							lam_standardMoveTest(targetTilePosition,testPosition,pieceValidMoveFlags_general);
						}
					}

					break;
				}
							 //////////////////////////////////////////////////////////////////////////
				case bishop: {
					lam_rayMoveTest(targetTileIndex,1,1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,1,-1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,-1,1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,-1,-1,7,pieceValidMoveFlags_general);
					break;
				}
							 //////////////////////////////////////////////////////////////////////////
				case queen: {
					lam_rayMoveTest(targetTileIndex,1,0,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,0,1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,-1,0,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,0,-1,7,pieceValidMoveFlags_general);

					lam_rayMoveTest(targetTileIndex,1,1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,1,-1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,-1,1,7,pieceValidMoveFlags_general);
					lam_rayMoveTest(targetTileIndex,-1,-1,7,pieceValidMoveFlags_general);
					break;
				}
							//////////////////////////////////////////////////////////////////////////
				case king: {
					static const int8 pieceMoveOffsets[8][2] = {
						{0,1},
						{1,0},
						{0,-1},
						{-1,0},
						{1,-1},
						{-1,1},
						{-1,-1},
						{1,1},
					};

					for(uint8 testMoveIndex = 0; testMoveIndex < 8; testMoveIndex++) {
						const TBoardPosition testPosition = targetTilePosition.getWithOffset(pieceMoveOffsets[testMoveIndex][0],pieceMoveOffsets[testMoveIndex][1]);

						if(testPosition.isValid()) {
							lam_standardMoveTest(targetTilePosition,testPosition,pieceValidMoveFlags_general);
						}
					}

					break;
				}
			}
		}
	}
}
