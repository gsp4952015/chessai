#pragma once

#include "Types.h"

#include <memory>

//#include "SpinLock.h"

namespace chess {

	enum class TTranspostionTableSetValueResult {
		ValueSet,
		ValueSet_WithFlagCollision,
		ValueSet_WithDepthCollision,
		ValueSet_WithQuiescenceCollision,
		ValueSet_WithIndexCollision,
		ValueNotSet_OldValueIsBetter,
		ValueNotSet_WithIndexCollision,
	};

	template<uint32 NumElementsMax>
	class transposition_table {
		static_assert(((NumElementsMax & (NumElementsMax-1)) == 0) && (NumElementsMax != 0),"NumElementsMax must be a power of 2!");

	private:
		struct TTableValue {
			//ht_spinlock mutex;
			uint32 keyUpper;
			uint16 birthPly;
			TTranspositionTableData data;
		};

		static const uint32 _ItemsPerChunk = 4194304; // must be a multiple of 2
		static const int32 _NumChunks = NumElementsMax / _ItemsPerChunk + (NumElementsMax % _ItemsPerChunk > 0 ? 1 : 0);

		//std::unique_ptr<TTableValue[]> tableData;
		std::unique_ptr<TTableValue[]> tableChunks[_NumChunks];

		inline TTableValue& _getItemAtIndex(int32 index) { return tableChunks[index / _ItemsPerChunk][index % _ItemsPerChunk]; }
		inline const TTableValue& _getItemAtIndex(int32 index) const { return tableChunks[index / _ItemsPerChunk][index % _ItemsPerChunk]; }

	public:
		transposition_table() {
			//tableData.reset(new TTableValue[NumElementsMax]);
			for(int32 chunk = 0; chunk < _NumChunks; chunk++) {
				tableChunks[chunk].reset(new TTableValue[_ItemsPerChunk]);
			}
			for(int32 i = 0; i < NumElementsMax; i++) {
				_getItemAtIndex(i).keyUpper = 0;
			}
		}

		~transposition_table() = default;
		transposition_table(const transposition_table&) = delete;

		inline TTranspostionTableSetValueResult setValue(const uint64 hash,const uint16 rootPly,const uint16 currentPly,const TTranspositionTableData& data) {
			const uint32 keyLower = hash & (NumElementsMax-1);
			const uint32 keyUpper = (uint64)(hash & ~(uint64)(NumElementsMax-1)) >> 32;

			//ht_spinlock_guard mtx_tableValue(tableData[tableIndex].mutex);

			TTranspostionTableSetValueResult resultFlag;
			bool shouldSetvalue;

			TTableValue& itemRef = _getItemAtIndex(keyLower);

			if(itemRef.keyUpper == 0) {
				resultFlag = TTranspostionTableSetValueResult::ValueSet;
				shouldSetvalue = true;
			} else if(itemRef.keyUpper == keyUpper) {
				if(data.flag == TTranspositionTableData::within_window && itemRef.data.flag != TTranspositionTableData::within_window) {
					resultFlag = TTranspostionTableSetValueResult::ValueSet_WithFlagCollision;
					shouldSetvalue = true;
				} else if(!data.isFromQuiescence && itemRef.data.isFromQuiescence) {
					resultFlag = TTranspostionTableSetValueResult::ValueSet_WithQuiescenceCollision;
					shouldSetvalue = true;
				} else if(data.depth > itemRef.data.depth) {
					resultFlag = TTranspostionTableSetValueResult::ValueSet_WithDepthCollision;
					shouldSetvalue = true;
				} else {
					shouldSetvalue = false;
					resultFlag = TTranspostionTableSetValueResult::ValueNotSet_OldValueIsBetter;
				}
			} else {
				if(rootPly >= itemRef.birthPly) {
					resultFlag = TTranspostionTableSetValueResult::ValueSet_WithIndexCollision;
					shouldSetvalue = true;
				} else {
					resultFlag = TTranspostionTableSetValueResult::ValueNotSet_WithIndexCollision;
					shouldSetvalue = false;
				}
			}

			if(shouldSetvalue) {
				itemRef.keyUpper = keyUpper;
				itemRef.birthPly = currentPly;
				itemRef.data = data;
			}

			return resultFlag;
		}

		inline bool probe(const uint64 hash,TTranspositionTableData* data) {
			const uint32 keyLower = hash & (uint64)(NumElementsMax-1);
			const uint32 keyUpper = (uint64)(hash & ~(uint64)(NumElementsMax-1)) >> 32;

			//ht_spinlock_guard mtx_tableValue(tableData[tableIndex].mutex);

			TTableValue& itemRef = _getItemAtIndex(keyLower);

			if(itemRef.keyUpper == keyUpper) {
				if(data != nullptr) *data = itemRef.data;
				return true;
			} else {
				return false;
			}
		}
	};

}