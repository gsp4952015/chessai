#include "PlayerInput.h"


chess::player_input_interface::player_input_interface() {
	wantsInput = false;
}

bool chess::player_input_interface::needsInput() const {
	std::lock_guard<decltype(mtx)> lck(mtx);
	return wantsInput;
}

chess::piece_color chess::player_input_interface::colorThatNeedsInput() const {
	std::lock_guard<decltype(mtx)> lck(mtx);
	return inputColor;
}

bool chess::player_input_interface::pushInput(const std::string& inputString) {
	std::lock_guard<decltype(mtx)> lck(mtx);
	if(wantsInput) {
		prom_requestInput.set_value(inputString);
		prom_requestInput.swap(decltype(prom_requestInput)()); // reset the promise
		wantsInput = false;
		return true;
	}
	return false;
}

std::shared_future<std::string> chess::player_input_interface::requestInput(piece_color color) {
	std::lock_guard<decltype(mtx)> lck(mtx);
	assert(!wantsInput);
	wantsInput = true;
	inputColor = color;
	return prom_requestInput.get_future();
}
