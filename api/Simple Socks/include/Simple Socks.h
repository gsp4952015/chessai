#pragma once
#include <memory>

/**
* The SimpleSocks namespace encloses the full public interface.
*/
namespace SimpleSocks {
  /**
  * \brief Initializes the library.
  *
  * Internally activates an automatic object which calls WSAStartup() upon
  * activation and WSACleanup() upon deactivation or deconstruction. This
  * function should be called once at the start of the program, but the user
  * may choose to manually call WSAStartup() instead.\n
  * This function uses version 2.2 of winsock.
  *
  * @return
  * * -1 on error.
  * * 0 on success.
  */
  int initSimpleSocks();
  /**
  * \brief Shuts down the library.
  * 
  * Internally calls WSACleanup() by deactivating the object triggered by
  * initSimpleSocks(). If the init function was not used then this function
  * will do nothing.
  */
  void shutdownSimpleSocks();

  /**
  * \brief A class for using a TCP connection.
  *
  * An instance of TCPSocket represents the local end of a socket
  * connection.
  */
  class TCPSocket {
  public:
    TCPSocket();
    ~TCPSocket();

    /**
    * \brief Attempt to connect to 'host' on 'port'.
    *
    * The 'connect' function attempts to establish a connection to a remote
    * host. If the function fails the state of the object is not changed.
    * The function will fail immediately if the socket is already 
    * explicitly connected. Upon success the object is considered
    * explicitly connected.
    *
    * example -> int errcode = sock.connect("www.google.com", 80);\n
    * example -> int errcode = sock.connect("127.0.0.1", 23);
    *
    * @param host A string containing an IPv4 address or the canonical name of an internet host.
    * @param port The port number to connect to on the remote host.
    * @return
    * * -1 on error
    * * 0 on success
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int connect(const char* host, unsigned short port);

    /**
    * \brief Close the connection.
    *
    * Explicitly disconnects the object, returning it to its (re-usable)
    * initial state. Note that this is the only function which explicitly
    * closes the socket. If the connection is closed from the remote end it
    * will not be explicitly closed. You must call this function before
    * re-using the object.
    */
    void close();

    /**
    * \brief Send 'length' bytes beginning at 'buffer' to connected machine.
    *
    * This function will block if the hardware/driver outbound buffer is
    * full. Do not assume that on success the full body of data was sent.\n
    * Check the returned value.
    *
    * @param buffer The starting address of the data to be sent.
    * @param length The byte-length of the data to be sent.
    * @return
    * * -1 on error.
    * * 0 if the connection was closed remotely.
    * * -2 if the socket is in non-blocking mode and the call would block.
    * * Otherwise returns the number of bytes which were sent.
    */
    int send(const char* buffer, int length);

    /**
    * \brief Receive up to 'length' bytes to 'buffer'.
    *
    * Will block until data is received unless in non-blocking mode. If
    * length of incoming data exceeds 'length' then only the first 'length'
    * bytes will be pulled in from the hardware/driver buffer. The
    * remaining bytes will wait in that buffer. On success, do not assume
    * that the full number of bytes in 'length' were read.\n
    * Check the returned value.
    *
    * @param buffer The start address of the buffer to write the incoming data into.
    * @param length The maximum number of bytes you wish to read. Should not exceed the size of the buffer pointed to by 'buffer'.
    * @return
    * * -1 on error.
    * * 0 if the socket was closed on the remote end.
    * * -2 if in non-blocking mode and no data was present.
    * * Otherwise returns the number of bytes read into the buffer.
    */
    int recv(char* buffer, int length);

    /**
    * \brief Peek ahead at received data.
    *
    * This function works exactly as 'recv' does except that internally it
    * uses the MSG_PEEK flag, which prevents the received data from being
    * removed from the hardware/driver buffer. Using 'peek' before 'recv'
    * can be useful for things such as finding the length of an HTTP header
    * or etc.
    *
    * @param buffer The start address of the buffer to write the incoming data into.
    * @param length The maximum number of bytes you wish to read. Should not exceed the size of the buffer pointed to by 'buffer'.
    * @return
    * * -1 on error.
    * * 0 if the socket was closed on the remote end.
    * * -2 if in non-blocking mode and no data was present.
    * * Otherwise returns the number of bytes read into the buffer.
    */
    int peek(char* buffer, int length);

    /**
    * \brief Indicates blocking mode status.
    *
    * @return
    * * true if the socket is operating in blocking mode.
    * * false if not connected or if in non-blocking mode.
    */
    bool getBlocking() const;

    /**
    * \brief Sets the blocking mode of a connected socket.
    *
    * Effects the use of send/recv/peek functions. If in blocking mode
    * these functions will wait until they can be completed before
    * returning. In non-blocking mode they will always return immediately.
    * The default state of a socket is blocking. The blocking state will
    * reset if the socket is closed and re-opened.\n
    * The function will fail if the socket is not explicitly connected.
    *
    * @param block Set true for blocking mode or false for non-blocking mode.
    * @return
    * * -1 on error.
    * * 0 on success.
    */
    int setBlocking(bool block);

    /**
    * \brief Indicates whether the socket is explicitly connected.
    *
    * The explicit connection state is determined ONLY by calls to
    * 'connect()' and 'close()'. Remote closures and socket errors
    * (implicit closures) may invalidate the socket handle being used by
    * the object, in which case all communication calls will return -1
    * until the object is reset. This function will NOT indicate that
    * state. You must detect such errors yourself and reset the object.
    * This function only indicates the explicit connection state set by
    * 'close()' and 'connect()'.
    *
    * @return
    * * true if explicitly connected.
    * * false if explicitly closed or not connected.
    */
    bool isConnected() const;

  private:
    TCPSocket(TCPSocket&);
    void operator=(TCPSocket&);
    struct impl;
    std::auto_ptr<impl> pimpl;
  };

  /**
  * \brief A class for accepting incoming TCP connections.
  *
  * A TCPServer object binds a local port and listens for incoming
  * connections on that port. You can accept an incoming connection by
  * passing an unconnected TCPSocket object to an 'accept()' call on a
  * listening TCPServer object. The passed in TCPSocket will be linked to
  * the incoming connection and may then be used normally.
  */
  class TCPServer {
  public:
    TCPServer();
    ~TCPServer();

    /**
    * \brief Begin listening for connections on indicated port.
    *
    * This function explicitly starts the object. The optional argument
    * 'localHostAddr' can be used on a system that has more than one
    * network adapter (called a "multihomed" system) to indicate which
    * device should be listened to. The default is 'all devices'. The
    * function will fail if the object is already listening.\n
    * When a binding is released it may take a few minutes for the
    * system to actually free the port for re-binding. Use the
    * 'forceBind' option to force the port to be re-used in such cases,
    * but be aware that this can lead to unexpected behavior if not
    * handled carefully.
    *
    * example -> int errcode = serv.start(23);\n
    * example -> int errcode = serv.start(80, false, "192.168.0.50");
    *
    * @param port The port number to listen to.
    * @param forceBind An optional bool specifying whether or not to force the binding.
    * @param localHostAddr An optional string containing an IPv4 address indicating the local device to listen to.
    * @return
    * * -1 on error.
    * * 0 on success.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int start(unsigned short port, bool forceBind = false, const char* localHostAddr = NULL);

    /**
    * \brief Stops listening and closes the server.
    *
    * Explicitly stops the object, returning it to its (re-usable)
    * initial state.
    */
    void stop();

    /**
    * \brief Accepts an incoming connection.
    *
    * This function hands off an incoming connection to an unconnected
    * TCPSocket object. The passed-in object will then be connected to the
    * remote client and may be used normally. Passing in an already
    * connected object will return an error. If the server is in blocking
    * mode this function will wait until an incoming connection is
    * available. Otherwise it will return immediately, indicating the
    * result in the return code. Also note that the socket connected by
    * a successful accept() call defaults to the blocking mode of the
    * server: If the server is non-blocking then the TCPSockets it
    * connects will also be non-blocking. Otherwise they will be in
    * blocking mode. (You can still change them later.)
    *
    * Please note that this function is overloaded and can accept a
    * TCPSocket object either by reference or by pointer.
    *
    * @param sock A disconnected TCPSocket object to associate with the incoming connection.
    * @return
    * * -1 on error.
    * * 0 on success, indicating that the passed-in object is now connected.
    * * -2 if in non-blocking mode and no incoming connection was available.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int accept(TCPSocket& sock);
    /**
    * @see accept(TCPSocket& sock);
    */
    int accept(TCPSocket* sock);

    /**
    * \brief Indicates whether the server is explicitly running.
    *
    * The explicit run state is determined ONLY by calls to 'start()' and 
    * 'stop()'. Internal failures may invalidate the server handle being
    * used by the object, in which case 'accept()' calls will return -1 
    * until the object is reset. This function will NOT indicate that
    * state. You must detect such errors yourself and reset the object.
    * This function only indicates the explicit connection state set by
    * 'start()' and 'stop()'.
    *
    * @return
    * * true if explicitly running.
    * * false if explicitly stopped or not started.
    */
    bool isRunning() const;

    /**
    * \brief Indicates blocking mode status.
    *
    * @return
    * * true if the server is operating in blocking mode.
    * * false if not connected or if in non-blocking mode.
    */
    bool getBlocking() const;

    /**
    * \brief Sets the blocking mode of a running server.
    *
    * Effects the use of accept function. If in blocking mode that function
    * will wait until it can be completed before returning. In non-blocking
    * mode it will always return immediately. The default state of a server
    * is blocking. The blocking state will reset to true if the server is
    * stopped and re-started.\n
    * The function will fail if the server is not explicitly running.
    *
    * @param block Set true for blocking mode or false for non-blocking mode.
    * @return
    * * -1 on error.
    * * 0 on success.
    */
    int setBlocking(bool block);

  private:
    TCPServer(TCPServer&);
    void operator=(TCPServer&);
    struct impl;
    std::auto_ptr<impl> pimpl;
  };

  /**
  * \brief This struct simplifies the use of the UDPSocket class.
  *
  * The members are stored in network byte order.\n
  * The 'addr' member is equivalent to the s_addr member of the in_addr
  * structure in the Winsock architecture.
  */
  struct IPv4Addr {
    unsigned long addr; /**< Host address */
    unsigned short port; /**< Host port number */
  };

  /**
  * \brief A class for managing a UDP socket.
  *
  * Functions in the UDPSocket class will return -2 to indicate an
  * 'incorrect mode' error if the requested call is not valid for the
  * current mode.
  */
  class UDPSocket {
  public:
    UDPSocket();
    ~UDPSocket();

    /**
    * \brief Enumerates the possible modes of a UDP socket object.
    *
    * * UDP_INACTIVE  - Socket is inactive and ready to be bound or connected.\n
    * * UDP_CONNECTED - Socket is connected to a specific host.\n
    * * UDP_BOUND     - Socket is bound to a local port in connectionless mode.
    */
    enum UDPMode {
      UDP_INACTIVE,
      UDP_CONNECTED,
      UDP_BOUND
    };

    /**
    * \brief As advertised.
    */
    UDPMode getMode() const;

    /**
    * \brief Returns bound local port number.
    *
    * Used in UDP_BOUND or UDP_CONNECTED modes.\n
    * If the UDPSocket is placed in UDP_CONNECTED mode without an explicit
    * binding it will bind an "ephemeral" port. This function is the only
    * means of ascertaining the number of that port.
    *
    * @return
    * * 0 on error or if in UDP_INACTIVE mode.
    * * On success, the number of the currently bound port.
    */
    unsigned short getBoundPort() const;

    /**
    * \brief Disconnects and un-binds socket.
    *
    * Returns object to UDP_INACTIVE mode. Any binding will be released and
    * any connection state will be cancelled. Be aware that SO_REUSEADDR is
    * not used by this library, so attempting to re-bind a recently used
    * port before it times-out will likely fail.
    */
    void reset();

    /**
    * \brief Binds the socket to a local port.
    *
    * Used in UDP_INACTIVE mode.\n
    * On success enters the UDP_BOUND mode.\n
    * When a binding is released it may take a few minutes for the
    * system to actually free the port for re-binding. Use the
    * 'forceBind' option to force the port to be re-used in such cases,
    * but be aware that this can lead to unexpected behavior if not
    * handled carefully.
    *
    * @param port The local port number to bind to the socket.
    * @param forceBind An optional bool specifying whether or not to force the binding.
    * @return
    * * -1 on failure.
    * * -2 if the mode is not UDP_INACTIVE.
    * * 0 on success.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int bind(unsigned short port, bool forceBind = false);

    /**
    * \brief Sends a datagram to the indicated host. (with name resolution)
    *
    * Used in UDP_BOUND mode.\n
    * The optional argument 'out_remoteAddr' will fill in the indicated
    * struct if the operation succeeds. The filled-in struct may then be
    * used with subsequent calls to sendto() in order to avoid the overhead
    * associated with name resolution/address translation. You may use a
    * length value of zero if you want to just resolve into out_remoteAddr
    * without actually sending anything.
    *
    * example -> IPv4Addr remAddr;\n
    * example -> int errcode = sendto("www.google.com", 21, "test", 4, &remAddr);\n
    * example -> errcode = sendto(&remAddr, "test again", 10);
    *
    * Keep in mind that UDP datagram sizes are fairly small. Success does 
    * not necessarily indicate that all bytes requested were sent. Be sure
    * to check the returned value.
    *
    * On failure the contents of 'out_remoteAddr' are not changed.
    *
    * @param host A string containing the IPv4 address or canonical host name of the target host.
    * @param port The remote host's port to send the data to.
    * @param buffer A pointer to the start of the data to be sent.
    * @param length The maximum number of bytes to send.
    * @param out_remoteAddr Optional argument pointing to an IPv4Addr object to be filled in with resolved host address information.
    * @return
    * * -1 on error.
    * * -2 if the mode is not UDP_BOUND.
    * * -3 if in non-blocking mode and the hardware/driver output buffer is full.
    * * On success, the number of bytes sent.
    */
    int sendto(const char* host, unsigned short port, const char* buffer, int length, IPv4Addr* out_remoteAddr = NULL);

    /**
    * \brief Sends a datagram to the indicated host. (without name resolution)
    *
    * Used in UDP_BOUND mode.\n
    * This version of the sendto() function accepts a filled-in IPv4Addr
    * struct indicating the target host. This can be used to avoid the
    * overhead of repreated name resolution/address translation.
    *
    * Otherwise it is identical to the other version of sendto().
    *
    * @param ipaddr A filled-in IPv4Addr indicating the target host and port.
    * @param buffer A pointer to the start of the data to be sent.
    * @param length The maximum number of bytes to send.
    * @return
    * * -1 on error.
    * * -2 if the mode is not UDP_BOUND.
    * * -3 if in non-blocking mode and the hardware/driver output buffer is full.
    * * On success, the number of bytes sent.
    */
    int sendto(const IPv4Addr* ipaddr, const char* buffer, int length);

    /**
    * \brief Accepts a datagram received on the bound port.
    *
    * Used in UDP_BOUND mode.\n
    * This function will accept any datagram received from any machine on
    * the currently bound port. On failure the ipaddr struct will not be
    * changed.
    *
    * @param ipaddr Pointer to an IPv4Addr object to be filled-in with the address information of the sender. (Set to NULL if not needed.)
    * @param buffer Pointer to a buffer in which to store the received data.
    * @param length The maximum number of bytes to receive. (Typically the length of the pointed-to buffer.)
    * @return
    * * -1 on error.
    * * -2 if the mode is not UDP_BOUND.
    * * -3 if in non-blocking mode and no incoming datagram was waiting.
    * * On success, the number of bytes that were received.
    */
    int recvfrom(IPv4Addr* ipaddr, char* buffer, int length);

    /**
    * \brief Associates with the indicated host, entering UDP_CONNECTED mode.
    *
    * Used in UDP_INACTIVE or UDP_BOUND mode.\n
    * If called from UDP_BOUND mode the bound socket will be retained as
    * the local port number. If called from UDP_INACTIVE mode an ephemeral
    * port number will be assigned.
    *
    * @see getBoundPort()
    *
    * This version of connect() performs name resolution/address
    * translation. If you already have a loaded IPv4Addr struct (from
    * recvfrom() or etc) use the other version to avoid the overhead of 
    * resolution/translation.
    *
    * In connected mode any datagram received on the bound port which does 
    * not originate from the associated(connected) address and port will be 
    * silently discarded.
    *
    * Note that the UDP protocol is a conntectionless protocol. In this 
    * context 'connection' simply indicates a default target for sending 
    * and filters out non-target datagrams upon receiving. Success in 
    * entering UDP_CONNECTED mode does not indicate an actual connection to
    * the remote host.
    *
    * @param host A string containing the canonical host name or IPv4 address of the host to connect to.
    * @param port The target port on the host machine.
    * @return
    * * -1 on failure.
    * * -2 if the mode is already UDP_CONNECTED.
    * * 0 on success.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int connect(const char* host, unsigned short port);

    /**
    * \brief Associates with the indicated host, entering UDP_CONNECTED mode.
    *
    * This is an alternative version of the connect() function which allows
    * you to bypass the overhead associated with hostname resolution or
    * address translation by providing a pre-loaded IPv4Addr struct. Apart
    * from that it is identical to the other version.
    *
    * @param ipaddr Pointer to an IPv4Addr struct containing the target host's address information.
    * @return
    * * -1 on failure.
    * * -2 if the mode is already UDP_CONNECTED.
    * * 0 on success.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int connect(IPv4Addr* ipaddr);

    /**
    * \brief Disconnects from the connected host, entering UDP_BOUND mode.
    *
    * Used in UDP_CONNECTED mode.\n
    * This function removes the connection-association, returning the 
    * socket to UDP_BOUND mode while retaining the currently bound port.
    *
    * @return
    * * -1 on error.
    * * -2 if not in UDP_CONNECTED mode.
    * * 0 on success.
    */
    int disconnect();

    /**
    * \brief Sends a datagram to the associated host.
    *
    * Used in UDP_CONNECTED mode.\n
    * Keep in mind that the datagram size in UDP mode is relatively small.
    * Success does not guarantee that the full data was sent. Be sure to
    * check the returned value.
    *
    * @param buffer A pointer to the beginning of the data to be sent.
    * @param length The maximum number of bytes to be sent.
    * @return
    * * -1 on error.
    * * -2 if not in UDP_CONNECTED mode.
    * * -3 if in non-blocking mode and the hardware/driver output buffer is full.
    * * On success, the number of bytes actually sent.
    */
    int send(const char* buffer, int length);

    /**
    * \brief Receives a datagram from the associated host.
    *
    * Used in UDP_CONNECTED mode.\n
    * Note that in connected mode only datagrams sent from the associated
    * host and port are received. Any other datagrams received on the bound
    * port are silently discarded. Keep in mind that success does not
    * indicate that the entire buffer has been filled. Remember to check
    * the return value. In UDP mode data is transferred in datagram-sized
    * chunks. If a datagram is too large to fit in the buffer provided to
    * recv() then the buffer will be filled to capacity and the remaining
    * data will be discarded. Avoid this by ensuring that the buffer is
    * large enough to contain a full datagram.
    *
    * @param buffer A pointer to the beginning of a buffer where the received data is to be written.
    * @param length The maximum number of bytes to receive. (Typically the length of the buffer.)
    * @return
    * * -1 on error.
    * * -2 if not in UDP_CONNECTED mode.
    * * -3 if in non-blocking mode and no incoming datagram was waiting.
    * * On success, the number of bytes written to the buffer.
    *
    */
    int recv(char* buffer, int length);

    /**
    * \brief Indicates blocking mode status.
    *
    * Used in UDP_BOUND or UDP_CONNECTED modes.
    *
    * @return
    * * true if the socket is operating in blocking mode.
    * * false if mode is UDP_INACTVE or if in non-blocking mode.
    */
    bool getBlocking() const;

    /**
    * \brief Sets the blocking mode of a connected socket.
    *
    * Used in UDP_CONNECTED or UDP_BOUND modes.\n
    * Effects the use of send/recv/sento/recvfrom functions. If in blocking
    * mode these functions will wait until they can be completed before
    * returning. In non-blocking mode they will always return immediately.
    * The default state of a socket is blocking. The blocking state will 
    * reset to true if the socket is reset.
    * The function will fail if the socket is in UDP_INACTIVE mode.
    *
    * @param block Set true for blocking mode or false for non-blocking mode.
    * @return
    * * -1 on error.
    * * 0 on success.
    */
    int setBlocking(bool block);

  private:
    UDPSocket(UDPSocket&);
    void operator=(UDPSocket&);
    struct impl;
    std::auto_ptr<impl> pimpl;
  };

  /**
  * \brief The SelectSet class allows the creation and management of sets of
  * objects for use with the 'select()' function.
  */
  class SelectSet {
  public:
    SelectSet();
    ~SelectSet();

    /**
    * \brief Erases this set's contents and copies in the contents of 'src'.
    * 
    * This function allows you to store all of your objects in a 'master'
    * set and then create a copy in a 'working' set to select on. The
    * function is overloaded and can accept either a reference of a pointer.
    * 
    * @param src The set whose contents should replace those of this set.
    * @return
    * * 0 on success.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int copyFrom(SelectSet& src);

    /**
    * @see copyFrom(SelectSet& src)
    */
    int copyFrom(SelectSet* src);

    /**
    * \brief Returns the number of objects in the set.
    */
    int getCount() const;

    /**
    * \brief Erases this set's contents.
    */
    void clear();

    /**
    * \brief Removes all invalidated objects from the set.
    *
    * While the push functions will not allow an invalid object to be added
    * to the set, it is possible that an object is added and then becomes
    * invalid at a later point in time by being deactivated or placed back
    * into blocking mode. Calling this function will remove all such objects
    * from the set in one fell swoop, leaving behind only those objects
    * which are still valid. This can be useful if you're making a lot of
    * changes to set members and don't want to spend the time removing every
    * invalidated object individually. However, it is a bit heavier on the
    * CPU since it has to check the state of every object in the set.
    *
    * @return The number of objects remaining in the set after validation. (It's prudent to check and make sure the set isn't empty.)
    */
    int validate();

    /**
    * \brief Adds an object to this set.
    *
    * This function adds an object to the set. Overloads exist for all three
    * object types: TCPSocket, TCPServer and UDPSocket.
    *
    * An object must meet certain criteria to be accepted into the set:\n
    * * TCPSocket - must be connected and non-blocking.
    * * TCPServer - must be running and non-blocking.
    * * UDPSocket - must be UDP_BOUND or UDP_CONNECTED and non-blocking.
    *
    * @param sock/serv A pointer to the object to be added to the set.
    * @return
    * * -1 on error (the object to be added does not meet criteria)
    * * 0 on success.
    * * -5 on internal allocation error (equivalent to std::bad_alloc)
    */
    int pushObject(TCPSocket* sock);
    /**
    * @see pushObject(TCPSocket* sock);
    */
    int pushObject(TCPServer* serv);
    /**
    * @see pushObject(TCPSocket* sock);
    */
    int pushObject(UDPSocket* sock);

    /**
    * \brief Removes a specific object from the set.
    *
    * This function is overloaded to accept any of the 3 valid object types.
    *
    * @param sock/serv A pointer to the object to be removed.
    */
    void removeObject(TCPSocket* sock);
    /**
    * @see removeObject(TCPSocket* sock);
    */
    void removeObject(TCPServer* serv);
    /**
    * @see removeObject(TCPSocket* sock);
    */
    void removeObject(UDPSocket* sock);

    /**
    * \brief Removes a TCPSocket from this set and returns a pointer to the
    * removed object.
    * 
    * This function should be used after the set has been submitted to the
    * select() function. TCPSocket objects remaining in the set at that
    * point have data waiting to be read.
    *
    * @return
    * * A pointer to a TCPSocket object with pending data to be read.
    * * NULL if no more TCPSocket objects remain in the set.
    */
    TCPSocket* popTCPSocket();

    /**
    * \brief Removes a TCPServer from this set and returns a pointer to the
    * removed object.
    * 
    * This function should be used after the set has been submitted to the
    * select() function. TCPServer objects remaining in the set at that
    * point have incoming connections waiting to be accepted.
    *
    * @return
    * * A pointer to a TCPServer object with at least one waiting connection.
    * * NULL if no more TCPServer objects remain in the set.
    */
    TCPServer* popTCPServer();

    /**
    * \brief Removes a UDPSocket from this set and returns a pointer to the
    * removed object.
    * 
    * This function should be used after the set has been submitted to the
    * select() function. UDPSocket objects remaining in the set at that
    * point have data waiting to be read.
    *
    * @return
    * * A pointer to a UDPSocket object with pending data to be read.
    * * NULL if no more UDPSocket objects remain in the set.
    */
    UDPSocket* popUDPSocket();
  private:
    SelectSet(SelectSet&);
    SelectSet& operator=(SelectSet&);
    struct impl;
    std::auto_ptr<impl> pimpl;
  };

  /**
  * \brief This enumerated value can be used as a timeout value to make the
  * 'select()' function block indefinately until at least one member of its
  * test set is ready.
  *
  * @see select()
  */
  enum {SELECT_FOREVER = ~0};

  /**
  * \brief Crop a SelectSet such that the only objects it contains are ones
  * which have incoming data ready.
  *
  * This function is used to determine which objects are ready to be
  * interacted with. By passing in a set of non-blocking objects the set
  * will have all objects which are not ready removed from it, so that the
  * objects which remain are only those which have pending interactions of
  * some sort.
  *
  * @see SelectSet
  *
  * @param set A pointer to a SelectSet that has been loaded with active, non-blocking objects to be tested.
  * @param timeout The number of milliseconds to wait for a ready object. Use 0 to return immediately, or SELECT_FOREVER to block the thread until at least one object is ready.
  * @return 
  * * -1 on error (probably an invalid object in the set)
  * * Otherwise the number of ready objects in the set (may be zero)
  */
  int select(SelectSet* set, unsigned int timeout);
};
