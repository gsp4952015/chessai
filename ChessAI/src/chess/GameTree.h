#pragma once

#include "Types.h"

#include "TranspositionTable.h"
#include "HistoryTable.h"

#include <vector>
#include <atomic>

#include "SpinLock.h"

namespace chess {
	class board;

	// search
	class GameTree {
	private:
		enum { Infinity = INT32_MAX };

		bool initialized;

		struct {
			engine_duration statusUpdateInterval;
		} settings;

		struct {
			uint16 deepestDepthSearched;
			uint64 numNodes;
			uint64 numNodseInQuiescence;
			uint64 numCutoffs;
			uint64 numLeaves;
			uint64 timesHitDepthCap;
			uint16 trans_maxDepth;
			uint64 num_transHits;
			uint64 num_transMisses;
			uint64 num_transMisses_fromQuiescence;
			uint64 num_valueSet;
			uint64 num_valueSet_WithFlagCollision;
			uint64 num_valueSet_WithDepthCollision;
			uint64 num_valueSet_WithQuiescenceCollision;
			uint64 num_valueSet_WithIndexCollision;
			uint64 num_valueNotSet_OldValueIsBetter;
			uint64 num_valueNotSet_WithIndexCollision;
			uint64 num_transNotDeepEnough;
			uint64 num_transFailLow;
			uint64 num_transFailHigh;
			uint64 num_transAccept;
		} stats;

		struct {
			struct TNodeInfo {
				uint8 numChildrenToSearch;
				uint8 numChildrenAlreadySearched;

				TNodeInfo() :
					numChildrenToSearch(0),
					numChildrenAlreadySearched(0) {
				}
			};

			mutable ht_spinlock mtx_displayData;
			int32 iterationDisplay;
			std::vector<TNodeInfo> nodeInfoVec;
			uint8 maximumDepth;
			uint8 iterationsRemaining;

			bool canQuitFromOutOfTime;
			TBoardMove bestMoveAtRoot;
			const board* rootBoardState;
			transposition_table<33554432> transpositionTable;
			history_table historyTable;
			std::atomic_bool outOfTime;
		} runtimeData;

		void _updateStatusDisplay() const;

		int32 _mtdf(const board& boardState,
					const piece_color color,
					const int32 firstGuess,
					const uint8 minimumSearchDepth);

		int32 _negamax(const board& boardState,
					   const piece_color color,
					   int32 alpha,
					   int32 beta,
					   uint8 depthRemaining,
					   const uint8 actualHeight = 0,
					   const bool isQuiescenceActive = false,
					   int8 qui_treeCheckQuiescenceBudget = 2);
	public:
		GameTree();

		void initialize(engine_duration statusUpdateInterval);

		struct SearchResult {
			TBoardMove chosenMove;
			int32 moveValue;
		};
		SearchResult getBestMove(const board& boardState,
								 const uint8 minimumSearchDepth,
								 const uint8 maximumSearchDepth,
								 const piece_color color,
								 const engine_duration timeLimit);
	};
}