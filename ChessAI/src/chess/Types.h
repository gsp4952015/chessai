#pragma once

#include "HTBitField.h"
#include <type_traits>

#include <chrono>
//typedef std::chrono::high_resolution_clock                    engine_clock;
//typedef std::chrono::duration<double>                         engine_duration;
//typedef std::chrono::time_point<engine_clock,engine_duration> engine_time_point;
typedef std::chrono::system_clock engine_clock;
typedef engine_clock::duration engine_duration;
typedef engine_clock::time_point engine_time_point;

namespace chess {
	// primitive types
	//////////////////////////////////////////////////////////////////////////
	typedef       unsigned char uint8;
	typedef                 char int8;
	typedef     unsigned short uint16;
	typedef               short int16;
	typedef      unsigned long uint32;
	typedef                long int32;
	typedef unsigned long long uint64;
	typedef           long long int64;

	// piece enums
	//////////////////////////////////////////////////////////////////////////
	enum piece_color : uint8 {
		white = 0,
		black = 1
	};
	static const piece_color NextColor[2] = {black,white};
	static const int8 ColorMultiplier[2] = {1,-1};
	const char* const piece_color_name[] = {
		"white",
		"black",
	};

	enum piece_type : uint8 {
		none = 0,
		pawn,
		knight,
		bishop,
		rook,
		queen,
		king,

		num_pieces
	};
	const char* const piece_type_name[num_pieces] = {
		"none",
		"pawn",
		"knight",
		"bishop",
		"rook",
		"queen",
		"king",
	};
	enum piece_quality : uint8 {
		pq_none = 0,
		pq_pawn,
		pq_minor,
		pq_major,
		pq_king,

		pq_num
	};
	static piece_quality PieceQualityMap[num_pieces] = {
		pq_none,  // none
		pq_pawn,  // pawn
		pq_minor, // knight
		pq_minor, // bishop
		pq_major, // rook
		pq_major, // queen
		pq_king   // king
	};

	enum move_mode : uint8 {
		mm_invalid = 0,
		mm_reposition,
		mm_reposition_double_pawn_push,
		mm_castle_kingside,
		mm_castle_queenside,
		mm_capture,
		mm_capture_en_passant,
		mm_promotion_queen,
		mm_promotion_knight,
		mm_promotion_queen_capture,
		mm_promotion_knight_capture,

		mm_pawn_can_capture, // for use in determining area of influence
		mm_defense,

		mm_num
	};
	static const bool IsUnderpromotionEnabled = false;
	static const uint16 MoveModeFlags_All = ~decltype(MoveModeFlags_All)(0) >> ((sizeof(MoveModeFlags_All)*8) - mm_num);
	static const uint16 MoveModeFlags_CaptureOnly = decltype(MoveModeFlags_CaptureOnly)((1 << mm_capture) | (1 << mm_promotion_queen_capture) | (1 << mm_promotion_knight_capture));
	static const uint16 MoveModeFlags_AllMakable = MoveModeFlags_All & ~((1 << mm_defense) | (1 << mm_pawn_can_capture));

	// castling
	namespace castling {
		enum CastlingSide : uint8 {
			queen = 0,
			king
		};

		static const uint8 ValidCastlingRookLocationFrom_kingside[2] = {
			7, // white
			63 // black
		};

		static const uint8 ValidCastlingRookLocationTo_kingside[2] = {
			5, // white
			61 // black
		};

		static const uint8 ValidCastlingRookLocationFrom_queenside[2] = {
			0, // white
			56 // black
		};

		static const uint8 ValidCastlingRookLocationTo_queenside[2] = {
			3, // white
			59 // black
		};

		static const uint8 ValidCastlingKingLocationFrom[2] = {
			4, // white
			60 // black
		};

		static const uint8 ValidCastlingKingLocationTo_kingside[2] = {
			6, // white
			62 // black
		};

		static const uint8 ValidCastlingKingLocationTo_queenside[2] = {
			2, // white
			58 // black
		};
	}

	// en passant
	namespace en_passant {
		static const uint8 EnPassantEngaugementRank[2] = {
			4, // white
			3, // black
		};

		static const uint8 EnPassantVulnurabilityRank_FromTo[2][2] = {
			{1,3}, // white
			{6,4}, // black
		};
	}

	namespace evaluation_data {
		struct TPersonality {
			struct {
				float staticMod;
				float material;
				float positional;
				float piece;
				float threat;
				float mobility;
			} weights;
		};

		static const float AIPersonalitiesRandomVariation[2] = {0.075f,0.075f};
		extern TPersonality AIPersonalities[2];

		// applied to a color's evaluation score when they move the same piece type repeatedly (excluding pawns and captures)
		static const uint8 RepeatedPieceMoveLogSize = 6;
		static const int16 RepeatedPieceMovePenality[RepeatedPieceMoveLogSize] = {0,0,-10,-50,-200,-500};

		// queen-side/king-side
		static const int16 HasCastlingRightsBonus[2] = {150,175};
		static const int16 HasAlreadyCastledBonus = 300;

		static const uint16 MaterialValues[num_pieces] = {
			0,    // none
			100,  // pawn
			320,  // knight
			330,  // bishop
			500,  // rook
			900,  // queen
			30000 // king
		};

		static const int16 KingAttackWeights[num_pieces] = {
			0,  // none
			0,  // pawn
			2,  // knight
			5,  // bishop
			7,  // rook
			10, // queen
			0   // king
		};

		// bonus applied when the color possesses all pieces of a given type
		static const int16 AllPiecesBonus[num_pieces] = {
			0,  // none
			0,  // pawn
			10, // knight
			10, // bishop
			10, // rook
			0,  // queen
			0   // king
		};

		static const int16 PieceTotalCount[num_pieces] = {
			0, // none
			8, // pawn
			2, // knight
			2, // bishop
			2, // rook
			1, // queen
			1  // king
		};

		// adjust piece values based on number of own pawns
		static const int16 MaterialAdj_Knight_NumPawns[9] = {-20,-16,-12,-8,-4,0,4,8,12};
		static const int16 MaterialAdj_Rook_NumPawns[9] = {15,12,9,6,3,0,-3,-6,-9};

		static const int16 KingRingAttackMobilityMultiplier = 2;
		static const int16 KingRingFriendlyOccupantsBonus[9] = {-250,-20,10,15,20,21,22,23,24};
		static const int16 KingRingEnemyOccupantsBonus[9] = {0,-10,-20,-25,-28,-30,-35,-40,-50};

		// bonus to pieces based on which pieces are threatening each other
		// ThreatBonus[attacking quality][attacked quality][weak/defended]
		static const int16 ThreatPenalty[pq_num][pq_num][2] = {
			{ // none
				{0,0},	// none
				{0,0},	// pawn
				{0,0},	// minor
				{0,0},	// major
				{0,0},	// king
			},
			{ // pawn
				{0,0},	// none
				{-10,0},	// pawn
				{10,5},	// minor
				{-195,-95},	// major
				{0,0},	// king
			},
			{ // minor
				{0,0},	// none
				{-10,0},	// pawn
				{-20,-5},	// minor
				{-50,-25},	// major
				{0,0},	// king
			},
			{ // major
				{0,0},	// none
				{-10,0},	// pawn
				{-30,0},	// minor
				{-40,-5},	// major
				{0,0},	// king
			},
			{ // king
				{0,0},	// none
				{-10,50},	// pawn
				{-20,50},	// minor
				{-40,50},	// major
				{0,0},	// king
			},
		};

		// bonus to pieces based on which pieces are threatening each other
		// ThreatBonus[defending quality][defended quality][weak/threatened]
		static const int16 DefenseBonus[pq_num][pq_num][2] = {
			{ // none
				{0,0},	// none
				{0,0},	// pawn
				{0,0},	// minor
				{0,0},	// major
				{0,0},	// king
			},
			{ // pawn
				{0,0},	// none
				{5,10},	// pawn
				{5,10},	// minor
				{10,20},	// major
				{-10,0},	// king
			},
			{ // minor
				{0,0},	// none
				{5,10},	// pawn
				{5,20},	// minor
				{15,50},	// major
				{-10,0},	// king
			},
			{ // major
				{0,0},	// none
				{5,10},	// pawn
				{10,15},	// minor
				{20,30},	// major
				{-10,0},	// king
			},
			{ // king (try to get the king to stay away from threatened pieces)
				{0,0},	// none
				{2,-10},	// pawn
				{2,-10},	// minor
				{2,-10},	// major
				{0,0},	// king
			},
		};

		static const int16 PawnRankBonus[8] = {0,0,10,20,30,40,60,0};
		static const int16 PawnCanPromoteBonus = 350;

		// from the perspective of white, invert the index: actualIndex = color==white ? givenIndex : 63-givenIndex
		// TODO: Expand the PositionValueTables to include refined values for different game stages (earlyGame, midGame, lateGame)
		static const int16 PositionValueTables[num_pieces][64] = {
			{ // none //////////////////////////////////////////////////////////////////////////
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,
			},
			{ // pawn //////////////////////////////////////////////////////////////////////////
				0,0,0,0,0,0,0,0,
				50,50,50,50,50,50,50,50,
				10,10,20,30,30,20,10,10,
				5,5,10,25,25,10,5,5,
				0,0,0,20,20,0,0,0,
				5,-5,-10,0,0,-10,-5,5,
				5,10,10,-20,-20,10,10,5,
				0,0,0,0,0,0,0,0
			},
			{ // knight //////////////////////////////////////////////////////////////////////////
				-50,-40,-30,-30,-30,-30,-40,-50,
				-40,-20,0,0,0,0,-20,-40,
				-30,0,10,15,15,10,0,-30,
				-30,5,15,20,20,15,5,-30,
				-30,0,15,20,20,15,0,-30,
				-30,5,10,15,15,10,5,-30,
				-40,-20,0,5,5,0,-20,-40,
				-50,-40,-30,-30,-30,-30,-40,-50,
			},
			{ // bishop //////////////////////////////////////////////////////////////////////////
				-20,-10,-10,-10,-10,-10,-10,-20,
				-10,0,0,0,0,0,0,-10,
				-10,0,5,10,10,5,0,-10,
				-10,5,5,10,10,5,5,-10,
				-10,0,10,10,10,10,0,-10,
				-10,10,10,10,10,10,10,-10,
				-10,5,0,0,0,0,5,-10,
				-20,-10,-10,-10,-10,-10,-10,-20,
			},
			{ // rook //////////////////////////////////////////////////////////////////////////
				0,0,0,0,0,0,0,0,
				5,10,10,10,10,10,10,5,
				-5,0,0,0,0,0,0,-5,
				-5,0,0,0,0,0,0,-5,
				-5,0,0,0,0,0,0,-5,
				-5,0,0,0,0,0,0,-5,
				-5,0,0,0,0,0,0,-5,
				0,0,0,5,5,0,0,0
			},
			{ // queen //////////////////////////////////////////////////////////////////////////
				-20,-10,-10,-5,-5,-10,-10,-20,
				-10,0,0,0,0,0,0,-10,
				-10,0,5,5,5,5,0,-10,
				-5,0,5,5,5,5,0,-5,
				0,0,5,5,5,5,0,-5,
				-10,5,5,5,5,5,0,-10,
				-10,0,5,0,0,0,0,-10,
				-20,-10,-10,-5,-5,-10,-10,-20
			},
			{ // king //////////////////////////////////////////////////////////////////////////
				-30,-40,-40,-50,-50,-40,-40,-30,
				-30,-40,-40,-50,-50,-40,-40,-30,
				-30,-40,-40,-50,-50,-40,-40,-30,
				-30,-40,-40,-50,-50,-40,-40,-30,
				-20,-30,-30,-40,-40,-30,-30,-20,
				-10,-20,-20,-20,-20,-20,-20,-10,
				20,20,0,0,0,0,20,20,
				20,30,10,0,0,10,30,20
			}
		};
	}

	static const int32 ForfeitScoreThreshold = -(int32)(evaluation_data::MaterialValues[king] * 0.75f);

	// board data
	//////////////////////////////////////////////////////////////////////////
	HTBitFieldTypeBegin(TBoardData,88);

	HTBitFieldArrayBegin(tiles,0,11,64);
	HTBitFieldArrayMember(pieceColor,uint8,0,1);
	HTBitFieldArrayMember(pieceType,uint8,1,3);
	HTBitFieldArrayMember(hasMovedAtAll,bool,4,1);

// 	HTBitFieldArrayMember(_isThreatenedByWhite,bool,5,1);
// 	HTBitFieldArrayMember(_isDefendedByWhite,bool,6,1);
// 	HTBitFieldArrayMember(_isThreatenedByBlack,bool,7,1);
// 	HTBitFieldArrayMember(_isDefendedByBlack,bool,8,1);

	HTBitFieldArrayMember(_cheapestAttackerType_white,uint8,5,3);
	HTBitFieldArrayMember(_cheapestAttackerType_black,uint8,8,3);

// 	inline bool isThreatenedByColor(const piece_color color) const { return color == white ? *_isThreatenedByWhite : *_isThreatenedByBlack; }
// 	inline void set_isThreatenedByColor(const piece_color color,bool threatened) { (color == white ? *(_isThreatenedByWhite = threatened) : *(_isThreatenedByBlack = threatened)); }
// 	inline bool isDefendedByColor(const piece_color color) const { return color == white ? *_isDefendedByWhite : *_isDefendedByBlack; }
// 	inline void set_isDefendedByColor(const piece_color color,bool defended) { (color == white ? *(_isDefendedByWhite = defended) : *(_isDefendedByBlack = defended)); }

	inline uint8 getCheapestAttackerType(const uint8 color) const;
	inline void setCheapestAttackerType(const uint8 color,const uint8 type);
	inline void addAttacker(const uint8 color,const uint8 type);

	inline bool isClear() const { return pieceType == none; }
	inline bool hasEnemy(const uint8 friendlyColor) const { return !isClear() && pieceColor != friendlyColor; }
	inline bool hasAlly(const uint8 friendlyColor) const { return !isClear() && pieceColor == friendlyColor; }
	HTBitFieldArrayEnd();

	HTBitFieldTypeEnd();
	inline uint8 TBoardData::T_tiles_ElementAccessor::getCheapestAttackerType(const uint8 color) const {
		// only want to keep track of the cheapest attacker
		if(color == white) {
			return static_cast<piece_type>(*_cheapestAttackerType_white);
		} else {
			return static_cast<piece_type>(*_cheapestAttackerType_black);
		}
	}
	inline void TBoardData::T_tiles_ElementAccessor::setCheapestAttackerType(const uint8 color,const uint8 type) {
		// only want to keep track of the cheapest attacker
		if(color == white) {
			_cheapestAttackerType_white = type;
		} else {
			_cheapestAttackerType_black = type;
		}
	}
	inline void TBoardData::T_tiles_ElementAccessor::addAttacker(const uint8 color,const uint8 type) {
		// only want to keep track of the cheapest attacker
		if(color == white) {
			const uint16 curAttackerValue = evaluation_data::MaterialValues[_cheapestAttackerType_white];
			if(evaluation_data::MaterialValues[type] > curAttackerValue) _cheapestAttackerType_white = type;
		} else {
			const uint16 curAttackerValue = evaluation_data::MaterialValues[_cheapestAttackerType_black];
			if(evaluation_data::MaterialValues[type] > curAttackerValue) _cheapestAttackerType_black = type;
		}
	}

	/*struct TBoardData {
		struct TTile {
			piece_color pieceColor;
			piece_type pieceType;
			bool hasMovedAtAll;
			bool vulnerableToEnPassant;
			bool _isThreatenedByWhite;
			bool _isDefendedByWhite;
			bool _isThreatenedByBlack;
			bool _isDefendedByBlack;

			inline bool isThreatenedByColor(const piece_color color) const { return color == white ? _isThreatenedByWhite : _isThreatenedByBlack; }
			inline void set_isThreatenedByColor(const piece_color color,bool threatened) { (color == white ? (_isThreatenedByWhite = threatened) : (_isThreatenedByBlack = threatened)); }
			inline bool isDefendedByColor(const piece_color color) const { return color == white ? _isDefendedByWhite : _isDefendedByBlack; }
			inline void set_isDefendedByColor(const piece_color color,bool defended) { (color == white ? (_isDefendedByWhite = defended) : (_isDefendedByBlack = defended)); }

			inline bool isClear() const { return pieceType == none; }
			inline bool hasEnemy(const piece_color friendlyColor) const { return !isClear() && pieceColor != friendlyColor; }
			inline bool hasAlly(const piece_color friendlyColor) const { return !isClear() && pieceColor == friendlyColor; }
		};

		TTile tiles[64];
	};*/

	// potential moves (returned from move generation function)
	// absolute maximum number of potential moves is 321
	//     9 queens (27 moves) + 2 rooks(14 moves) + 2 bishops(13 moves) + 2 knights(8 moves) + 1 king(8 moves)
	//    NOTE: in practice, this should never get above 100
	static const uint32 MaximumNumPossibleMoves = 100;
	HTBitFieldTypeBegin(TBoardPossibleMoves,202);

	HTBitFieldMember(numMoves,uint8,0,7);

	HTBitFieldArrayBegin(moves,7,16,MaximumNumPossibleMoves);
	union {
		HTBitFieldArrayMember(x,uint8,0,3);
		HTBitFieldArrayMember(y,uint8,3,3);
	} from;

	union {
		HTBitFieldArrayMember(x,uint8,6,3);
		HTBitFieldArrayMember(y,uint8,9,3);
	} to;

	HTBitFieldArrayMember(mode,uint8,12,4);
	HTBitFieldArrayEnd();

	HTBitFieldTypeEnd();

	// positional
	//////////////////////////////////////////////////////////////////////////

	// board position
	// can be implicitly converted into a uint8 for use with board class functions
	HTBitFieldTypeBegin(TBoardPosition,1);

	HTBitFieldMember(x,uint8,0,3);
	HTBitFieldMember(y,uint8,3,3);

	HTBitFieldMember(isInvalid,bool,7,1);

	inline TBoardPosition(const uint8 x,const uint8 y);
	inline TBoardPosition getWithOffset(const int8& offsetX,const int8& offsetY) const;
	inline bool isValid() const;
	HTBitFieldTypeEnd();
	static const TBoardPosition InvalidPosition = 0x80;
	inline TBoardPosition::TBoardPosition(const uint8 x,const uint8 y) {
		*this = (y<<3) + x;
	}
	inline bool TBoardPosition::isValid() const {
		return *this != InvalidPosition;
	}
	inline TBoardPosition TBoardPosition::getWithOffset(const int8& offsetX,const int8& offsetY) const {
		const uint8 newX = (int8)x + offsetX;
		const uint8 newY = (int8)y + offsetY;
		if(newX < 8 && newY < 8) {
			return TBoardPosition(newX,newY);
		}
		return InvalidPosition;
	}

	// board move
	HTBitFieldTypeBegin(TBoardMove,2);

	union {
		HTBitFieldMember(x,uint8,0,3);
		HTBitFieldMember(y,uint8,3,3);
	} from;

	union {
		HTBitFieldMember(x,uint8,6,3);
		HTBitFieldMember(y,uint8,9,3);
	} to;

	HTBitFieldMember(mode,uint8,12,4);

	inline TBoardMove(const TBoardPosition from,const TBoardPosition to,const uint8 mode);
	inline TBoardPosition getFromPosition() const { return TBoardPosition(from.x,from.y); }
	inline TBoardPosition getToPosition() const { return TBoardPosition(to.x,to.y); }
	inline bool isValid() const;

	// mode queries
	inline bool isQuiet() const;
	inline bool isACastle() const;
	inline bool isACapture() const;
	inline bool isAPromotion() const;

	HTBitFieldTypeEnd();
	static const TBoardMove InvalidMove = 0;
	inline TBoardMove::TBoardMove(const TBoardPosition from,const TBoardPosition to,const uint8 mode) {
		*this = uint16(from) | (uint16(to) << 6) | (uint16(mode) << 12);
	}
	inline bool TBoardMove::isValid() const {
		return *this != InvalidMove;
	}
	inline bool TBoardMove::isQuiet() const {
		const uint8 m = mode;
		return
			mode == mm_reposition ||
			mode == mm_reposition_double_pawn_push;
	}
	inline bool TBoardMove::isACastle() const {
		const uint8 m = mode;
		return
			mode == mm_castle_queenside ||
			mode == mm_castle_kingside;
	}
	inline bool TBoardMove::isACapture() const {
		const uint8 m = mode;
		return
			mode == mm_capture ||
			mode == mm_capture_en_passant ||
			mode == mm_promotion_queen_capture ||
			mode == mm_promotion_knight_capture;
	}
	inline bool TBoardMove::isAPromotion() const {
		const uint8 m = mode;
		return
			mode == mm_promotion_queen ||
			mode == mm_promotion_knight ||
			mode == mm_promotion_queen_capture ||
			mode == mm_promotion_knight_capture;
	}

	// HASHING
	//////////////////////////////////////////////////////////////////////////
	namespace hashing {
		enum TranspositionHashSpecialTypes : uint32 {
			thst_blacks_turn = 0,

			thst_num
		};

		extern uint64 _Hashes_TileState[2][num_pieces][64];
		extern uint64 _Hashes_EnPassantVulnurability[2][8];
		extern uint64 _Hashes_CastlingRights[2][2];
		extern uint64 _Hashes_Special[thst_num];

		void _generateHashValuesIfNecessary();
	}

	struct TTranspositionTableData {
		enum node_type : uint8 {
			within_window = 0,
			fail_low,
			fail_high,

			num
		};

		bool isFromQuiescence;
		node_type flag;
		TBoardMove pvMove;
		uint8 depth;
		int32 score;
	};
}