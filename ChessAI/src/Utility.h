#pragma once

#define NOMINMAX
#include <algorithm>

#define NEXT_MULTIPLE_OF_8(n) ((n+7) & ~7)

#define BIT_SET_TRUE(val,bit) val |= (1 << bit)
#define BIT_CLEAR(val,bit) val &= ~(1 << bit)
#define BIT_SET_VALUE(val,bit,value) BIT_CLEAR(val,bit); if(value) BIT_SET_TRUE(val,bit)
#define BIT_TOGGLE(val,bit) val ^= (1 << bit)
#define BIT_IS_SET(val,bit) ((val & (1 << bit)) != 0)

#define MAXFAST(a,b) ((a)>(b) ? (a) : (b))
#define MINFAST(a,b) ((a)<(b) ? (a) : (b))

#define SIGN(n) ((n)<0 ? -1:1)

inline int NumberOfSetBits32(int i) {
	// all hail the black magic of: http://stackoverflow.com/questions/109023/how-to-count-the-number-of-set-bits-in-a-32-bit-integer
	i = i - ((i >> 1) & 0x55555555);
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

inline int NumberOfSetBits64(unsigned long long i) {
	int* asDwords = reinterpret_cast<int*>(&i);
	return NumberOfSetBits32(asDwords[0]) + NumberOfSetBits32(asDwords[1]);
}

#include <random>
extern std::default_random_engine rand_generator;
typedef std::default_random_engine::result_type TRandomInteger;

inline TRandomInteger rand_getint() { return rand_generator(); }
inline TRandomInteger rand_getint(TRandomInteger min,TRandomInteger max) { return (rand_getint() % (max+1 - min)) + min; }

inline float rand_getfloat() { return rand_generator()/(float)rand_generator.max(); }
inline float rand_getfloat(float min,float max) { return rand_getfloat()*(min-max) + max; }

// BEGIN CODE FROM: http://stackoverflow.com/questions/14861018/center-text-in-fixed-width-field-with-stream-manipulators-in-c (Kevin Ballard)
#include <string>
#include <iostream>
#include <iomanip>

template<typename charT,typename traits = std::char_traits<charT> >
class center_helper {
	std::basic_string<charT,traits> str_;
public:
	center_helper(std::basic_string<charT,traits> str) : str_(str) {}
	template<typename a,typename b>
	friend std::basic_ostream<a,b>& operator<<(std::basic_ostream<a,b>& s,const center_helper<a,b>& c);
};

template<typename charT,typename traits = std::char_traits<charT> >
center_helper<charT,traits> centered(std::basic_string<charT,traits> str) {
	return center_helper<charT,traits>(str);
}

// redeclare for std::string directly so we can support anything that implicitly converts to std::string
inline center_helper<std::string::value_type,std::string::traits_type> centered(const std::string& str) {
	return center_helper<std::string::value_type,std::string::traits_type>(str);
}

template<typename charT,typename traits>
std::basic_ostream<charT,traits>& operator<<(std::basic_ostream<charT,traits>& s,const center_helper<charT,traits>& c) {
	std::streamsize w = s.width();
	if(w > (int)c.str_.length()) {
		std::streamsize left = (w + c.str_.length()) / 2;
		s.width(left);
		s << c.str_;
		s.width(w - left);
		s << "";
	} else {
		s << c.str_;
	}
	return s;
}
// END CODE FROM: http://stackoverflow.com/questions/14861018/center-text-in-fixed-width-field-with-stream-manipulators-in-c (Kevin Ballard)
