#include "Board.h"

void chess::board::resetBoard() {
	hashing::_generateHashValuesIfNecessary();

	auto lam_setTile = [this](uint8 x,uint8 y,piece_color color,piece_type type)->void {
		auto& tile = getTileStateModifier(x,y);
		tile.pieceColor = color;
		tile.pieceType = type;
	};

	memset(boardData.data,0,boardData.BufferSize);

	memset(movePenaltyHistory,0,sizeof(movePenaltyHistory));
	movePenaltyHistoryInsertionIndex = 0;

	memset(gameHashHistory,0,sizeof(gameHashHistory));
	gameHashHistoryInsertionIndex = 0;

	isColorInCheck[white] = isColorInCheck[black] = false;
	colorMobility[white] = colorMobility[black] = 0;
	hasAlreadyCastled[white] = hasAlreadyCastled[black] = false;
	currentPly = 0;

	for(uint8 color = 0; color < 2; color++)
		for(uint8 type = 0; type < 2; type++)
			castlingRights[color][type] = true;

	for(uint8 color = 0; color < 2; color++)
		for(uint8 file = 0; file < 8; file++)
			fileIsVulnurableToEnPassant[color][file] = false;

	lam_setTile(0,0,white,rook);
	lam_setTile(1,0,white,knight);
	lam_setTile(2,0,white,bishop);
	lam_setTile(3,0,white,queen);
	lam_setTile(4,0,white,king);
	lam_setTile(5,0,white,bishop);
	lam_setTile(6,0,white,knight);
	lam_setTile(7,0,white,rook);

	lam_setTile(0,1,white,pawn);
	lam_setTile(1,1,white,pawn);
	lam_setTile(2,1,white,pawn);
	lam_setTile(3,1,white,pawn);
	lam_setTile(4,1,white,pawn);
	lam_setTile(5,1,white,pawn);
	lam_setTile(6,1,white,pawn);
	lam_setTile(7,1,white,pawn);

	lam_setTile(0,7,black,rook);
	lam_setTile(1,7,black,knight);
	lam_setTile(2,7,black,bishop);
	lam_setTile(3,7,black,queen);
	lam_setTile(4,7,black,king);
	lam_setTile(5,7,black,bishop);
	lam_setTile(6,7,black,knight);
	lam_setTile(7,7,black,rook);
 
 	lam_setTile(0,6,black,pawn);
 	lam_setTile(1,6,black,pawn);
 	lam_setTile(2,6,black,pawn);
 	lam_setTile(3,6,black,pawn);
 	lam_setTile(4,6,black,pawn);
 	lam_setTile(5,6,black,pawn);
 	lam_setTile(6,6,black,pawn);
 	lam_setTile(7,6,black,pawn);

	/*lam_setTile(4,0,white,king);
	lam_setTile(0,7,black,king);
	lam_setTile(0,6,black,pawn);

	lam_setTile(3,3,white,pawn);
	lam_setTile(7,4,white,pawn);
	lam_setTile(6,1,black,pawn);
	lam_setTile(7,0,white,rook);*/

	/*lam_setTile(1,5,black,king);
	lam_setTile(0,0,black,queen);

	lam_setTile(3,5,white,pawn);
	getTileStateModifier(3,5).hasMovedAtAll = true;
	lam_setTile(7,7,white,queen);
	lam_setTile(6,0,white,king);
	lam_setTile(6,1,white,rook);*/

	/*lam_setTile(0,0,white,king);
	lam_setTile(5,7,black,king);

	lam_setTile(0,6,white,rook);
	lam_setTile(1,5,white,rook);*/

	colorThatCanMove = white;

	/*lam_setTile(5,6,white,king);
	lam_setTile(7,6,black,king);
	//lam_setTile(4,7,black,rook);
	lam_setTile(0,6,white,pawn);
	lam_setTile(1,6,white,pawn);
	lam_setTile(1,1,black,queen);*/

	// update mobility
	applyMove(InvalidMove,amf_calculateDerivedData);

	recalculateTranspositionHash();

	auto whiteval = evaluate(white);
	auto blackval = evaluate(black);

	int garbel = 5;
}

void chess::board::recalculateTranspositionHash() {
	transpositionHash = 0;

	for(uint8 boardIndex = 0; boardIndex < 64; boardIndex++) {
		const auto curTile = getTileStateModifier(boardIndex);

		if(curTile.pieceType != none) {
			transpositionHash ^= hashing::_Hashes_TileState[curTile.pieceColor][curTile.pieceType][boardIndex];
		}
	}

	for(uint8 color = 0; color < 2; color++)
		for(uint8 type = 0; type < 2; type++)
			if(castlingRights[color][type]) transpositionHash ^= hashing::_Hashes_CastlingRights[color][type];

	for(uint8 color = 0; color < 2; color++)
		for(uint8 file = 0; file < 8; file++)
			if(fileIsVulnurableToEnPassant[color][file]) transpositionHash ^= hashing::_Hashes_EnPassantVulnurability[color][file];

	if(colorThatCanMove == black) transpositionHash ^= hashing::_Hashes_Special[hashing::thst_blacks_turn];
}
