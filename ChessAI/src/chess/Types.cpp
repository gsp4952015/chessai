#include "Types.h"

#include "Utility.h"
#include <random>

namespace chess {
	namespace hashing {
		uint64 _Hashes_TileState[2][num_pieces][64];
		uint64 _Hashes_EnPassantVulnurability[2][8];
		uint64 _Hashes_CastlingRights[2][2];
		uint64 _Hashes_Special[thst_num];

		void _generateHashValuesIfNecessary() {
			static bool _AreValuesGenerated = false;

			if(!_AreValuesGenerated) {
				static const uint32 _SEED = 0x634EFDD2ui32;

				std::mt19937_64 rng;
				rng.seed(_SEED);

				std::uniform_int_distribution<unsigned long long> dist(0ui64,~0ui64);

				for(uint8 pieceColor = 0; pieceColor < 2; pieceColor++) {
					// tile state
					for(uint8 pieceType = pawn; pieceType < num_pieces; pieceType++) {
						for(uint8 tileId = 0; tileId < 64; tileId++) {
							_Hashes_TileState[pieceColor][pieceType][tileId] = dist(rng);
						}
					}
					// en passant file
					for(uint8 file = 0; file < 8; file++) {
						_Hashes_EnPassantVulnurability[pieceColor][file] = dist(rng);
					}
					// castling rights
					for(uint8 rightType = 0; rightType < 2; rightType++) {
						_Hashes_CastlingRights[pieceColor][rightType] = dist(rng);
					}
				}

				// special
				for(uint32 i = 0; i < thst_num; i++) {
					_Hashes_Special[i] = dist(rng);
				}

				_AreValuesGenerated = true;
			}
		}
	}

	namespace evaluation_data {
		TPersonality AIPersonalities[2] = {};
	}

}