#pragma once

#include "Types.h"

namespace chess {

	class history_table {
	private:
		uint16 _tableData[2][4096];

	public:
		history_table() {
			for(uint8 color = 0; color < 2; color++)
				for(uint32 index = 0; index < 4096; index++)
					_tableData[color][index] = 0;
		}

		inline void matureValues() {
			for(uint8 color = 0; color < 2; color++)
				for(uint32 index = 0; index < 4096; index++)
					_tableData[color][index] = _tableData[color][index] >> 1;
		}

		inline uint16 getHistoryValue(const uint8 color,const TBoardMove move) const {
			const uint16 index = ((uint16)move) & 0xfff; // ignore the 'type' bits
			return _tableData[color][index];
		}

		inline void addHistoryValue(const uint8 color,const TBoardMove move,const uint16 valueToAdd) {
			const uint16 index = ((uint16)move) & 0xfff; // ignore the 'type' bits
			_tableData[color][index] += valueToAdd;
		}
	};

}