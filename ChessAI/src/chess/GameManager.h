#pragma once

#include <memory>
#include <array>
#include <string>

#include "Types.h"
#include "Board.h"
#include "GameTree.h"
#include "PlayerInput.h"

#include <atomic>

namespace chess {
	class game_manager;

	// Helpers
	//////////////////////////////////////////////////////////////////////////
	std::string encodeMoveToString(const TBoardMove move);
	TBoardMove decodeMoveFromString(const std::string& moveString,const piece_color color);

	// Controllers
	//////////////////////////////////////////////////////////////////////////
	class game_color_controller {
	protected:
		piece_color myColor;
		game_manager* gameManager;

	public:
		game_color_controller(piece_color myColor,game_manager* gameManager) : myColor(myColor),gameManager(gameManager) {};

		virtual TBoardMove getMove(const board& boardState) const = 0;
	};

	class game_color_player_controller : public game_color_controller {
		player_input_interface* inputInterface;

	public:
		game_color_player_controller(piece_color myColor,game_manager* gameManager,player_input_interface* inputInterface) : game_color_controller(myColor,gameManager),inputInterface(inputInterface) {};

		virtual TBoardMove getMove(const board& boardState) const override;
	};

	class game_color_ai_controller : public game_color_controller {
	private:
		mutable GameTree gameTree;

	public:
		game_color_ai_controller(piece_color myColor,game_manager* gameManager);;

		virtual TBoardMove getMove(const board& boardState) const override;
	};

	// Game Manager
	//////////////////////////////////////////////////////////////////////////
	class game_manager {
	private:
		board boardState;
		piece_color currentPlayer;
		uint32 moveNumber;
		TBoardMove lastMove;
		std::unique_ptr<game_color_controller> colorControllers[2];

		struct TMessageLogEntry {
			std::string text;
			bool colorEnabled;
			struct {
				unsigned foreground,background;
			} color;
		};
		std::array<TMessageLogEntry,5> messageLog;

		void _clearWindow() const;

		void _redefineAIPersonalities(float variationWhite,float variationBlack);

	public:
		player_input_interface inputInterface;

		void pushLogMessage(const std::string& message);
		void pushLogMessage(const std::string& message,unsigned fgColor,unsigned bgColor);

		void renderToConsole() const;

		void runGame(bool whiteIsPlayerControlled,bool blackIsPlayerControlled,std::atomic_bool* atm_gameTerminated);
	};
}