#include <iostream>

#include "chess\GameManager.h"

int main() {
	std::atomic<bool> atm_gameTerminated = false;

	chess::game_manager chessGameManager;

	std::thread thrd_chessGame([&]() {
		bool IsPlayerControlled[2];
		for(int color = 0; color < 2; color++) {
			try {
				std::cout << "is " << chess::piece_color_name[color] << " player controlled? [y/n]: ";
				char response;
				std::cin >> response;
				response = tolower(response);

				switch(response) {
					case 'y':
						IsPlayerControlled[color] = true;
						break;
					case 'n':
						IsPlayerControlled[color] = false;
						break;
					default:
						throw 0; // retry entry
				}
			} catch(...) {
				std::cout << "Error, invalid input!\n";
				color--;
			}
		}

		chessGameManager.runGame(IsPlayerControlled[0],IsPlayerControlled[1],&atm_gameTerminated);
	});

	thrd_chessGame.join();

	return 0;
}