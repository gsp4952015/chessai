#include "GameTree.h"
#include "Board.h"
#include "Utility.h"

#include <numeric>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>

#include <future>

#include "Utility.h"

namespace chess {
	GameTree::GameTree() {
		initialized = false;
	}

	void GameTree::initialize(engine_duration statusUpdateInterval) {
		settings.statusUpdateInterval = statusUpdateInterval;

		initialized = true;
	}

	GameTree::SearchResult GameTree::getBestMove(const board& boardState,
												 const uint8 minimumSearchDepth,
												 const uint8 maximumSearchDepth,
												 const piece_color color,
												 const engine_duration timeLimit) {
		assert(initialized);
		assert(minimumSearchDepth >= 1);
		assert(maximumSearchDepth >= minimumSearchDepth);

		runtimeData.maximumDepth = maximumSearchDepth;

		SearchResult result;
		result.chosenMove = InvalidMove;
		result.moveValue = 0;

		bool ___DEBUG_hasAccpetedAValue = false;

		runtimeData.rootBoardState = &boardState;

		runtimeData.historyTable.matureValues();
		runtimeData.outOfTime = false;

		engine_time_point tp_before = engine_clock::now();

		// MTD(f) with iterative deepening
		; {
			int32 firstGuess = 0;

			std::atomic<bool> atm_updateStatusThreadTerminated = false;
			auto fut_updateStatus = std::async([&]() {
				while(!atm_updateStatusThreadTerminated) {
					_updateStatusDisplay();
					std::this_thread::sleep_for(settings.statusUpdateInterval);
				}
			});

			memset(&stats,0,sizeof(stats)); // only care about the stats of the final iteration
			for(uint8 targetDepth = 1; targetDepth < maximumSearchDepth; targetDepth++) {
				; {
					ht_spinlock_guard lck(runtimeData.mtx_displayData);
					runtimeData.iterationsRemaining = maximumSearchDepth - targetDepth + 1;
					runtimeData.iterationDisplay = minimumSearchDepth - (maximumSearchDepth - runtimeData.iterationsRemaining);
				}

				int32 scoreThisIteration = 0;

				auto lam_acceptSearchData = [&]()->void {
					result.chosenMove = runtimeData.bestMoveAtRoot;
					result.moveValue = scoreThisIteration;
					___DEBUG_hasAccpetedAValue = true;
				};

				auto fut_thisIteration = std::async([&]() { scoreThisIteration = _mtdf(boardState,color,firstGuess,targetDepth); });

				if(fut_thisIteration.wait_until(tp_before + timeLimit) == std::future_status::timeout) {
					if(targetDepth > minimumSearchDepth) runtimeData.outOfTime = true; // DO NOT KICK OUT OF THE FIRST ITERATION, WE MUST RETURN A VALID MOVE NO MATTER WHAT
					fut_thisIteration.wait();
					if(targetDepth > minimumSearchDepth) {
						std::cout << "\nRan out of time, a full " << std::max<int32>((int32)targetDepth-1,1) << " plies were searched";
						break;
					} else {
						lam_acceptSearchData(); // the search will have been allowed to finish in full on the first iteration
					}
				} else lam_acceptSearchData();

				firstGuess = scoreThisIteration;
			}

			atm_updateStatusThreadTerminated = true;
			fut_updateStatus.get();
		}

		assert(___DEBUG_hasAccpetedAValue);

		engine_time_point tp_after = engine_clock::now();

		const float timeTakenInSeconds = (float)std::chrono::duration_cast<std::chrono::milliseconds>(tp_after-tp_before).count()/1000.0f;

		std::cout <<
			"\nStats:\n"
			"deepestDepthSearched: " << stats.deepestDepthSearched << "\n"
			"numNodes: " << stats.numNodes << "\n"
			"numNodseInQuiescence: " << stats.numNodseInQuiescence << "\n"
			"numCutoffs: " << stats.numCutoffs << "\n"
			"numLeaves: " << stats.numLeaves << "\n"
			"timesHitDepthCap: " << stats.timesHitDepthCap << "\n"
			"trans_maxDepth: " << stats.trans_maxDepth << "\n"
			"num_transHits: " << stats.num_transHits << "\n"
			"num_transMisses: " << stats.num_transMisses << "\n"
			"transposition hit/miss ratio: " << ((float)stats.num_transHits / (float)stats.num_transMisses) << "\n"
			"num_transMisses_fromQuiescence: " << stats.num_transMisses_fromQuiescence << "\n"
			"num_valueSet: " << stats.num_valueSet << "\n"
			"num_transFailLow: " << stats.num_transFailLow << "\n"
			"num_transNotDeepEnough: " << stats.num_transNotDeepEnough << "\n"
			"num_transFailHigh: " << stats.num_transFailHigh << "\n"
			"num_transAccept: " << stats.num_transAccept << "\n"
			"num_valueSet_WithFlagCollision: " << stats.num_valueSet_WithFlagCollision << "\n"
			"num_valueSet_WithDepthCollision: " << stats.num_valueSet_WithDepthCollision << "\n"
			"num_valueSet_WithQuiescenceCollision: " << stats.num_valueSet_WithQuiescenceCollision << "\n"
			"num_valueSet_WithIndexCollision: " << stats.num_valueSet_WithIndexCollision << "\n"
			"num_valueNotSet_OldValueIsBetter: " << stats.num_valueNotSet_OldValueIsBetter << "\n"
			"num_valueNotSet_WithIndexCollision: " << stats.num_valueNotSet_WithIndexCollision << "\n"
			"time taken: " << timeTakenInSeconds << " seconds\n"
			"positions searched per second: " << (uint64)(stats.numNodes/timeTakenInSeconds) << "\n"
			;

		return result;
	}

	void GameTree::_updateStatusDisplay() const {
		ht_spinlock_guard lck(runtimeData.mtx_displayData);

		std::stringstream ss;

		ss << std::setw(4) << (int)runtimeData.iterationDisplay;
		for(auto& nodeInfo : runtimeData.nodeInfoVec) {
			char buffer[20];
			sprintf_s(buffer,"%i",
					  (int)nodeInfo.numChildrenToSearch - (int)nodeInfo.numChildrenAlreadySearched);

			ss << std::setw(4) << buffer;
		}

		std::string dataString = ss.str();
		dataString.resize(79); // truncate the string to fit on one line
		std::cout << '\r' << std::string(79,' ') << '\r' << dataString;
	}

	chess::int32 GameTree::_mtdf(const board& boardState,
								 const piece_color color,
								 const int32 firstGuess,
								 const uint8 minimumSearchDepth) {
		int32 score = firstGuess;
		int32 bounds[2] = {-Infinity,Infinity};

		do {
			const int32 beta = score + (score==bounds[0]);
			score = _negamax(boardState,color,beta-1,beta,minimumSearchDepth);
			bounds[score < beta] = score;
		} while(bounds[0] < bounds[1]);

		return score;
	}

	int32 GameTree::_negamax(const board& boardState,
							 const piece_color color,
							 int32 alpha,
							 int32 beta,
							 uint8 depthRemaining,
							 const uint8 actualHeight,
							 const bool isQuiescenceActive,
							 int8 qui_treeCheckQuiescenceBudget) {
		if(runtimeData.outOfTime) return 0;

		stats.deepestDepthSearched = std::max<decltype(stats.deepestDepthSearched)>(stats.deepestDepthSearched,actualHeight);
		stats.numNodes++;
		if(isQuiescenceActive) stats.numNodseInQuiescence++;

		bool transpositionHit = false;
		TTranspositionTableData transpostionData;
		transpostionData.pvMove = InvalidMove;

		// try to find the current board state in the transposition table
		if(runtimeData.transpositionTable.probe(boardState.transpositionHash,&transpostionData)) {
			stats.trans_maxDepth = std::max<decltype(stats.trans_maxDepth)>(stats.trans_maxDepth,transpostionData.depth);

			stats.num_transHits++;
			transpositionHit = true;
			if(!transpostionData.isFromQuiescence || isQuiescenceActive) {
				if(transpostionData.depth >= depthRemaining) {
					switch(transpostionData.flag) {
						case TTranspositionTableData::within_window:
							if(!boardState.wouldMoveViolateThreefoldRule(transpostionData.pvMove)) {
								//if(actualHeight < 2) break;
								// are we the root node?
								if(actualHeight == 0) runtimeData.bestMoveAtRoot = transpostionData.pvMove;

								stats.num_transAccept++;
								stats.numLeaves++;
								stats.numCutoffs++;
								return transpostionData.score; // forward prune, fully seen before
							}
							break;
						case TTranspositionTableData::fail_low:
							stats.num_transFailLow++;
							alpha = std::max(alpha,transpostionData.score); // narrow the window
							break;
						case TTranspositionTableData::fail_high:
							stats.num_transFailHigh++;
							beta = std::min(beta,transpostionData.score); // narrow the window
							break;
					}

					if(alpha >= beta) {
						stats.numLeaves++;
						stats.numCutoffs++;
						if(actualHeight == 0) runtimeData.bestMoveAtRoot = transpostionData.pvMove; // TODO: remove this line
						return transpostionData.score; // forward prune, value is not within current window
					}
				} else stats.num_transNotDeepEnough++;
			} else stats.num_transMisses_fromQuiescence++;
		} else stats.num_transMisses++;

		// is this a horizon node?
		if((depthRemaining == 0 || actualHeight >= runtimeData.maximumDepth)) { // only stop of odd plies
			if(actualHeight >= runtimeData.maximumDepth) stats.timesHitDepthCap++;
			stats.numLeaves++;
			return boardState.evaluate(color);
		}

		int32 score = -Infinity;

		// quiescence

		if(isQuiescenceActive) {
			const int32 stand_pat = boardState.evaluate(color);
			if(stand_pat >= beta) {
				score = beta;
				stats.numCutoffs++;
				goto jmp_done;
			}
			alpha = std::min(alpha,stand_pat);
		}

		// generate and sort move list
		TBoardPossibleMoves allValidMoves;
		boardState.runMoveGenerator(&allValidMoves,color);
		uint8 numMovesToSearch = allValidMoves.numMoves;

		if(numMovesToSearch == 0) {
			stats.numLeaves++;
			return -Infinity; // no valid moves, mate/stalemate
		}

		uint8 moveOrder[MaximumNumPossibleMoves];

		// sort move list
		; {
			// sorting order
			// pvMove (from transposition table)
			// winning captures/promotions (static exchange evaluation)
			// equal captures/promotions (static exchange evaluation)
			// non captures sorted via history heuristic
			// losing captures (static exchange evaluation)
			int32 moveValues[MaximumNumPossibleMoves];
			uint8 numBadCaptureMovesToConsiderInQuiescence = 3;
			uint8 numBadNonCaptureMovesToConsiderInQuiescence = 1;
			uint8 numberOfMovesToTruncate = 0;
			// initialize data
			for(uint8 i = 0; i < numMovesToSearch; i++) {
				moveOrder[i] = i;
				const TBoardMove thisMove = (TBoardMove)allValidMoves.moves[i];
				moveValues[i] = runtimeData.historyTable.getHistoryValue(color,thisMove);

				// if quiescence is active, we ignore everything except captures and promotions
				// and even then, we only care about ones that have a static exchange evaluation that is favorable
				if(!isQuiescenceActive) {
					if(thisMove == transpostionData.pvMove) {
						// pvMove
						moveValues[i] = 1 << 30;
					} else if(thisMove.isACapture() || thisMove.isAPromotion()) {
						// winning/equal captures/promotions
						const int32 moveStaticEvaluation = boardState.seeMove(thisMove,color);
						moveValues[i] += moveStaticEvaluation << 15;
					}
				} else {
					if(thisMove.isACapture() || thisMove.isAPromotion()) {
						const int32 moveStaticEvaluation = boardState.seeMove(thisMove,color);
						if(moveStaticEvaluation > 0) {
							moveValues[i] += moveStaticEvaluation;
						} else {
							moveValues[i] = 0x80080000i32 + moveValues[i] + moveStaticEvaluation;
							if(numBadCaptureMovesToConsiderInQuiescence > 0) {
								numBadCaptureMovesToConsiderInQuiescence--;
							} else {
								numberOfMovesToTruncate++;
							}
						}
					} else {
						if(numBadNonCaptureMovesToConsiderInQuiescence > 0) {
							moveValues[i] = 0x80000800i32 + moveValues[i];
							numBadNonCaptureMovesToConsiderInQuiescence--;
						} else {
							moveValues[i] = 0xC0000000i32; // ensure move is at the end of the list
							numberOfMovesToTruncate++;
						}
					}
				}
			}

			// sort
			std::sort(moveOrder,moveOrder+numMovesToSearch,[&](const int32 a,const int32 b)->bool {
				return moveValues[a] > moveValues[b];
			});

			numMovesToSearch -= numberOfMovesToTruncate;
		}

		// FOR DISPLAY
		runtimeData.mtx_displayData.lock();
		runtimeData.nodeInfoVec.resize(actualHeight+1);
		runtimeData.nodeInfoVec[actualHeight].numChildrenToSearch = numMovesToSearch;
		runtimeData.nodeInfoVec[actualHeight].numChildrenAlreadySearched = 0;
		runtimeData.mtx_displayData.unlock();
		// FOR DISPLAY

		for(uint8 validMoveIndex = 0; validMoveIndex < numMovesToSearch; validMoveIndex++) {
			const TBoardMove childMove = (TBoardMove)allValidMoves.moves[moveOrder[validMoveIndex]];

			board childBoardState = boardState;
			childBoardState.applyMove(childMove);

			uint8 continuationDepth = depthRemaining-1;

			// quiescence
			bool shouldContinueInQuiescence = isQuiescenceActive;
			if(continuationDepth == 0) {
				if(!childMove.isQuiet()) {
					continuationDepth = std::max<uint8>(continuationDepth,2);
					shouldContinueInQuiescence = true;
				}
				if(qui_treeCheckQuiescenceBudget > 0 && (childBoardState.isColorInCheck[white] || childBoardState.isColorInCheck[black])) {
					qui_treeCheckQuiescenceBudget--;
					continuationDepth = std::max<uint8>(continuationDepth,3);
					shouldContinueInQuiescence = true;
				}
			}

			const int32 childValue = -_negamax(childBoardState,
											   NextColor[color],
											   -beta,
											   -std::max(alpha,score),
											   continuationDepth,
											   actualHeight+1,
											   shouldContinueInQuiescence,
											   qui_treeCheckQuiescenceBudget);

			if(childValue > score) {
				score = childValue;
				transpostionData.pvMove = childMove;

				if(score >= beta) {
					stats.numCutoffs++;
					if(!childMove.isACapture()) runtimeData.historyTable.addHistoryValue(color,childMove,depthRemaining*depthRemaining);
					goto jmp_done; // pruned
				}
			}

			// FOR DISPLAY
			runtimeData.mtx_displayData.lock();
			runtimeData.nodeInfoVec[actualHeight].numChildrenAlreadySearched++;
			runtimeData.mtx_displayData.unlock();
			// FOR DISPLAY
		}

		goto jmp_done;

		// DONE
		jmp_done:
		; {
			transpostionData.flag = TTranspositionTableData::within_window;

			if(score <= alpha)
				transpostionData.flag = TTranspositionTableData::fail_low;
			else if(score >= beta)
				transpostionData.flag = TTranspositionTableData::fail_high;

			transpostionData.depth = depthRemaining;
			transpostionData.score = score;

			transpostionData.isFromQuiescence = isQuiescenceActive;

			const auto ttableSetFlag = runtimeData.transpositionTable.setValue(boardState.transpositionHash,
																			   runtimeData.rootBoardState->currentPly,
																			   runtimeData.rootBoardState->currentPly + actualHeight,
																			   transpostionData);
			switch(ttableSetFlag) {
				case TTranspostionTableSetValueResult::ValueSet:
					stats.num_valueSet++;
					break;
				case TTranspostionTableSetValueResult::ValueSet_WithFlagCollision:
					stats.num_valueSet_WithFlagCollision++;
					break;
				case TTranspostionTableSetValueResult::ValueSet_WithDepthCollision:
					stats.num_valueSet_WithDepthCollision++;
					break;
				case TTranspostionTableSetValueResult::ValueSet_WithQuiescenceCollision:
					stats.num_valueSet_WithQuiescenceCollision++;
					break;
				case TTranspostionTableSetValueResult::ValueSet_WithIndexCollision:
					stats.num_valueSet_WithIndexCollision++;
					break;
				case TTranspostionTableSetValueResult::ValueNotSet_OldValueIsBetter:
					stats.num_valueNotSet_OldValueIsBetter++;
					break;
				case TTranspostionTableSetValueResult::ValueNotSet_WithIndexCollision:
					stats.num_valueNotSet_WithIndexCollision++;
					break;
			}

			// are we the root node?
			if(actualHeight == 0) runtimeData.bestMoveAtRoot = transpostionData.pvMove;

			return score;
		}
	}

}