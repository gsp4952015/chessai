#include "GameManager.h"

#include "GameTree.h"

#include "Utility.h" // centered() stream output helper
#include "color.h" // console colors

#define NOMINMAX
#include <numeric>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>

const unsigned COL_DIVIDER_LINE_FG = col_gray;
const unsigned COL_DIVIDER_CROSS_FG = col_dark_white;

const unsigned COL_PIECE_FG[2] = {col_white,col_green};
const unsigned COL_BOARD_BG[2] = {col_dark_blue,col_dark_red};
const unsigned COL_BOARD_BG_LastMove_From = col_dark_yellow;
const unsigned COL_BOARD_BG_LastMove_To = col_pink;

const unsigned COL_LABEL_FG_VERTICAL = col_red;
const unsigned COL_LABEL_BG_VERTICAL = col_black;
const unsigned COL_LABEL_FG_HORIZONTAL = col_red;
const unsigned COL_LABEL_BG_HORIZONTAL = col_black;

const unsigned COL_LOG_HEADER_FG = col_red;
const unsigned COL_LOG_HEADER_BG = col_black;
const unsigned COL_LOG_BG = col_black;
const unsigned COL_LOG_InCheck = col_red;
const unsigned COL_LOG_Victory = col_pink;

const unsigned CELL_SIZE = 3;

// Helpers
//////////////////////////////////////////////////////////////////////////
std::string chess::encodeMoveToString(const TBoardMove move) {
	switch(move.mode) {
		case mm_reposition:
		case mm_reposition_double_pawn_push:
		case mm_promotion_queen:
		case mm_promotion_knight:
		case mm_promotion_queen_capture:
		case mm_promotion_knight_capture:
		case mm_capture:
		case mm_capture_en_passant: {
			char buffer[5];
			sprintf_s(buffer,"%c%i%c%i",
					  'A' + (int)move.from.x,
					  (int)move.from.y+1,
					  'A' + (int)move.to.x,
					  (int)move.to.y+1);
			return buffer;

			break;
		}
		case mm_castle_queenside: {
			return "000";
		}
		case mm_castle_kingside: {
			return "00";
		}
	}

	assert(false); // move cannot be stringized

	return "ERROR";
}

chess::TBoardMove chess::decodeMoveFromString(const std::string& moveString,const piece_color color) {
	std::string moveString_sanitized = moveString;
	moveString_sanitized.erase(std::remove(moveString_sanitized.begin(),moveString_sanitized.end(),'-'),moveString_sanitized.end());
	std::replace(moveString_sanitized.begin(),moveString_sanitized.end(),'o','0');
	std::replace(moveString_sanitized.begin(),moveString_sanitized.end(),'O','0');

	auto lam_deciperGlyph = [&](char glyph)->uint8 {
		glyph = toupper(glyph);
		if(isdigit(glyph) && glyph >= '1' && glyph <= '8') {
			return glyph - '1';
		} else if(isalpha(glyph) && glyph >= 'A' && glyph <= 'H') {
			return glyph - 'A';
		} else {
			throw std::string("Invalid character: ") + glyph;
		}
	};

	const int32 numGlyphs = (int32)moveString_sanitized.size();

	if(numGlyphs == 4) {
		std::vector<uint8> decipheredGlyphs;
		for(int32 glyphNum = 0; glyphNum < numGlyphs; glyphNum++) {
			decipheredGlyphs.push_back(lam_deciperGlyph(moveString_sanitized[glyphNum]));
		}

		TBoardMove result = 0;

		result.from.x = decipheredGlyphs[0];
		result.from.y = decipheredGlyphs[1];
		result.to.x = decipheredGlyphs[2];
		result.to.y = decipheredGlyphs[3];
		result.mode = mm_invalid; // will be determined later

		return result;
	} else if(numGlyphs == 3) {
		if(moveString_sanitized[0] == '0' && moveString_sanitized[1] == '0' && moveString_sanitized[2] == '0') {
			TBoardMove result = uint16(castling::ValidCastlingRookLocationFrom_queenside[color]) | (uint16(castling::ValidCastlingRookLocationTo_queenside[color]) << 6);
			result.mode = mm_castle_queenside;
			return result;
		} else {
			throw "Invalid input!";
		}
	} else if(numGlyphs == 2) {
		if(moveString_sanitized[0] == '0' && moveString_sanitized[1] == '0') {
			TBoardMove result = uint16(castling::ValidCastlingRookLocationFrom_kingside[color]) | (uint16(castling::ValidCastlingRookLocationTo_kingside[color]) << 6);
			result.mode = mm_castle_kingside;
			return result;
		} else {
			throw "Invalid input!";
		}
	} else {
		throw "Invalid input!";
	}

	return InvalidMove;
}

// Controllers
//////////////////////////////////////////////////////////////////////////

// Player controller
//////////////////////////////////////////////////////////////////////////
chess::TBoardMove chess::game_color_player_controller::getMove(const board& boardState) const {
	using namespace std;

	while(true) {
		gameManager->renderToConsole();

		TBoardPossibleMoves allValidMoves;
		boardState.runMoveGenerator(&allValidMoves,myColor);

		if(allValidMoves.numMoves == 0) break;

		try {
			auto fut_inputRequest = inputInterface->requestInput(myColor);

			// asynchronously try to get input from the console, while other input methods (DirectX game board) can also fulfill the request
			std::async([&]() {
				int32 hintMoveIndex = rand_getint(0,allValidMoves.numMoves-1);
				cout << "Input move (ex: " << encodeMoveToString((TBoardMove)allValidMoves.moves[hintMoveIndex]) << "): ";
				/*for(int i = 0; i < allValidMoves.numMoves; i++) {
				cout << "i=" << i << ": " << encodeMoveToString((TBoardMove)allValidMoves.moves[i]) << '\n';
				}*/

				string response;
				getline(cin,response);

				inputInterface->pushInput(response);
			});

			std::string response = fut_inputRequest.get();

			TBoardMove enteredMove = decodeMoveFromString(response,myColor);

			bool isMoveValid = false;

			// if the type of the entered move is unknown, try to match it to one of the valid moves
			// but do not auto match a castling move
			if(enteredMove.mode == mm_invalid) {
				for(int32 i = 0; i < allValidMoves.numMoves; i++) {
					const auto curValidMove = allValidMoves.moves[i];
					if(curValidMove.mode != mm_castle_queenside &&
					   curValidMove.mode != mm_castle_kingside &&
					   curValidMove.from.x == enteredMove.from.x &&
					   curValidMove.from.y == enteredMove.from.y &&
					   curValidMove.to.x == enteredMove.to.x &&
					   curValidMove.to.y == enteredMove.to.y) {
						enteredMove = (uint16)curValidMove;
						isMoveValid = true;
						break;
					}
				}
			} else {
				for(int32 i = 0; i < allValidMoves.numMoves; i++) {
					if(allValidMoves.moves[i] == enteredMove) {
						isMoveValid = true;
						break;
					}
				}
			}

			if(isMoveValid) {
				return enteredMove;
			} else {
				throw "Invalid move!";
			}
		} catch(char* e) {
			std::stringstream ss;
			ss << "Error: " << e;
			gameManager->pushLogMessage(ss.str());
		} catch(std::string e) {
			std::stringstream ss;
			ss << "Error: " << e;
			gameManager->pushLogMessage(ss.str());
		}
	}

	return InvalidMove;
}

// AI Controller
//////////////////////////////////////////////////////////////////////////
chess::game_color_ai_controller::game_color_ai_controller(piece_color myColor,game_manager* gameManager) : game_color_controller(myColor,gameManager) {
	gameTree.initialize(std::chrono::milliseconds(75));
}

chess::TBoardMove chess::game_color_ai_controller::getMove(const board& boardState) const {
	printf("%s AI is thinking... mobility=%i",
		   piece_color_name[myColor],
		   boardState.colorMobility[myColor]);

	const auto searchResult = gameTree.getBestMove(boardState,6,32,myColor,std::chrono::seconds(10));
	TBoardMove moveToReturn = searchResult.chosenMove;

	char aiResultMessage[256];
	if(moveToReturn.isValid()) {
		if(searchResult.moveValue > ForfeitScoreThreshold) {
			sprintf_s(aiResultMessage,"%s AI chose move with value: %i",
					  piece_color_name[myColor],
					  searchResult.moveValue);
		} else {
			sprintf_s(aiResultMessage,"You've played a good game %s, I forfeit",
					  piece_color_name[NextColor[myColor]]);
			moveToReturn = InvalidMove;
		}
	} else {
		sprintf_s(aiResultMessage,"%s AI decided that victory was impossible, and forfeit",
				  piece_color_name[myColor]);
		moveToReturn = InvalidMove;
	}

	gameManager->pushLogMessage(aiResultMessage,COL_PIECE_FG[myColor],COL_LOG_BG);

	return moveToReturn;
}

// Game Manager
//////////////////////////////////////////////////////////////////////////
void chess::game_manager::_clearWindow() const {
	std::cout << std::string(100,'\n');
}

void chess::game_manager::_redefineAIPersonalities(float variationWhite,float variationBlack) {
	auto lam_getNewPersonalityValue = [](float variation)->float {
		return rand_getfloat(1-variation,1+variation);
	};

	const float colorVariation[2] = {variationWhite,variationBlack};

	for(uint8 color = 0; color < 2; color++) {
		evaluation_data::AIPersonalities[color].weights.staticMod = lam_getNewPersonalityValue(colorVariation[color]);
		evaluation_data::AIPersonalities[color].weights.material = lam_getNewPersonalityValue(colorVariation[color]);
		evaluation_data::AIPersonalities[color].weights.positional = lam_getNewPersonalityValue(colorVariation[color]);
		evaluation_data::AIPersonalities[color].weights.piece = lam_getNewPersonalityValue(colorVariation[color]);
		evaluation_data::AIPersonalities[color].weights.threat = lam_getNewPersonalityValue(colorVariation[color]);
		evaluation_data::AIPersonalities[color].weights.mobility = lam_getNewPersonalityValue(colorVariation[color]);
	}
}

void chess::game_manager::pushLogMessage(const std::string& message) {
	for(int32 i = (int32)messageLog.size()-2; i >= 0; i--) {
		messageLog[i+1] = messageLog[i];
	}
	messageLog[0].text = message;
	messageLog[0].colorEnabled = false;
}

void chess::game_manager::pushLogMessage(const std::string& message,unsigned fgColor,unsigned bgColor) {
	pushLogMessage(message);
	messageLog[0].color.foreground = fgColor;
	messageLog[0].color.background = bgColor;
	messageLog[0].colorEnabled = true;
}

void chess::game_manager::renderToConsole() const {
	using namespace std;

	//_clearWindow();

	// draw the log
	cout << sc::color(COL_LOG_HEADER_FG,COL_LOG_HEADER_BG) << "\n\n--- LOG:" << sc::colordef() << '\n';
	for(int32 i = (int32)messageLog.size() - 1; i >= 0; i--) {
		if(messageLog[i].colorEnabled)
			cout << sc::color(messageLog[i].color.foreground,messageLog[i].color.background) << messageLog[i].text << sc::colordef() << '\n';
		else
			cout << messageLog[i].text << '\n';
	}

	// draw the board
	for(uint8 drawY = 0; drawY < 9; drawY++) {
		const uint8 boardY = 7 - drawY;

		// horizontal divider line
		for(uint8 i = 0; i < 9; i++) {
			const char fillCharacter = (drawY == 0 && i == 0) ? ' ' : '-';
			cout << setfill(fillCharacter) << setw(CELL_SIZE) << sc::fgcolor(COL_DIVIDER_LINE_FG) << "" << sc::fgcolor(COL_DIVIDER_CROSS_FG) << "+" << sc::colordef() << setfill(' ');
		}
		cout << '\n';

		for(uint8 drawX = 0; drawX < 9; drawX++) {
			const uint8 boardX = drawX - 1;

			if(drawX == 0 && drawY < 8) { // vertical label
				char label[CELL_SIZE + 1];
				sprintf_s(label,"%i",boardY + 1);

				cout << setw(CELL_SIZE) << sc::fgcolor(COL_LABEL_FG_VERTICAL) << sc::bgcolor(COL_LABEL_BG_VERTICAL) << centered(label) << sc::colordef();
			} else if(drawY == 8) { // horizontal label
				if(drawX > 0) {
					char label[CELL_SIZE + 1];
					sprintf_s(label,"%c",'A' + boardX);

					cout << setw(CELL_SIZE) << sc::fgcolor(COL_LABEL_FG_HORIZONTAL) << sc::bgcolor(COL_LABEL_BG_HORIZONTAL) << centered(label) << sc::colordef();
				} else {
					// bottom left blank cell
					cout << setw(CELL_SIZE) << "";
				}
			} else if(boardX < 8 && boardY < 8) { // cell label
				static const char PIECE_LABELS[] = {
					' ', // none
					'P', // pawn
					'N', // knight
					'B', // bishop
					'R', // rook
					'Q', // queen
					'K', // king
				};

				auto tileState = boardState.getTileStateModifier(boardX,boardY);

				char label[CELL_SIZE + 1];
				sprintf_s(label,"%c",PIECE_LABELS[tileState.pieceType]);

				unsigned colorBoardBg = COL_BOARD_BG[(boardX + boardY) % 2];
				if(lastMove != InvalidMove) {
					if(lastMove.from.x == boardX && lastMove.from.y == boardY) {
						colorBoardBg = COL_BOARD_BG_LastMove_From;
					} else if(lastMove.to.x == boardX && lastMove.to.y == boardY) {
						colorBoardBg = COL_BOARD_BG_LastMove_To;
					}
				}

				cout << setw(CELL_SIZE) << sc::fgcolor(COL_PIECE_FG[tileState.pieceColor]) << sc::bgcolor(colorBoardBg) << centered(label) << sc::colordef();
			}

			cout << sc::fgcolor(COL_DIVIDER_LINE_FG) << "|" << sc::colordef();
		}

		cout << '\n';
	}
}

void chess::game_manager::runGame(bool whiteIsPlayerControlled,bool blackIsPlayerControlled,std::atomic_bool* atm_gameTerminated) {
	_redefineAIPersonalities(evaluation_data::AIPersonalitiesRandomVariation[white],evaluation_data::AIPersonalitiesRandomVariation[white]);

	auto lam_setupColorController = [&](piece_color color,bool playerControlled)->void {
		if(playerControlled)
			colorControllers[color].reset(new game_color_player_controller(color,this,&inputInterface));
		else
			colorControllers[color].reset(new game_color_ai_controller(color,this));
	};

	lam_setupColorController(white,whiteIsPlayerControlled);
	lam_setupColorController(black,blackIsPlayerControlled);

	for(auto& logEntry : messageLog) {
		logEntry.text = "";
		logEntry.colorEnabled = false;
	}

	boardState.resetBoard();
	currentPlayer = white;
	moveNumber = 0;
	lastMove = InvalidMove;

	engine_time_point tp_gameStart = engine_clock::now();

	while(!atm_gameTerminated->load()) {
		// add a message to the log if the player is in check
		if(boardState.isColorInCheck[currentPlayer]) {
			char logMessage[512];
			sprintf_s(logMessage,"%s is in check!",
					  piece_color_name[currentPlayer]);

			pushLogMessage(logMessage,COL_LOG_InCheck,COL_LOG_BG);
		}

		//_clearWindow();
		renderToConsole();

		const TBoardMove moveToMake = colorControllers[currentPlayer]->getMove(boardState);

		// check for move validity
		if(!moveToMake.isValid()) {
			char logMessage[512];
			sprintf_s(logMessage,"%s has defeated %s in %i moves",
					  piece_color_name[NextColor[currentPlayer]],
					  piece_color_name[currentPlayer],
					  moveNumber/2);

			pushLogMessage(logMessage,COL_LOG_Victory,COL_LOG_BG);

			renderToConsole();

			break;
		}

		// add a message to the log describing the move
		; {
			const auto tileFrom = boardState.getTileStateModifier(moveToMake.from.x,moveToMake.from.y);
			const auto tileTo = boardState.getTileStateModifier(moveToMake.to.x,moveToMake.to.y);

			char textCodeFrom[3];
			sprintf_s(textCodeFrom,"%c%i",
					  'A' + (int)moveToMake.from.x,
					  (int)moveToMake.from.y+1);
			char textCodeTo[3];
			sprintf_s(textCodeTo,"%c%i",
					  'A' + (int)moveToMake.to.x,
					  (int)moveToMake.to.y+1);

			char captureMessage[256] = {};
			if(tileTo.pieceType != none && tileTo.pieceColor != currentPlayer) {
				sprintf_s(captureMessage,", taking %s's %s",
						  piece_color_name[NextColor[currentPlayer]],
						  piece_type_name[tileTo.pieceType]);
			}

			char logMessage[512];
			sprintf_s(logMessage,"%s moved their %s(%s) to %s%s",
					  piece_color_name[currentPlayer],
					  piece_type_name[tileFrom.pieceType],
					  textCodeFrom,
					  textCodeTo,
					  captureMessage);

			pushLogMessage(logMessage,COL_PIECE_FG[currentPlayer],COL_LOG_BG);
		}

		boardState.applyMove(moveToMake);
		lastMove = moveToMake;

		moveNumber++;
		currentPlayer = NextColor[currentPlayer];
	}

	engine_time_point tp_gameEnd = engine_clock::now();

	std::cout << "Game completed in " << std::chrono::duration_cast<std::chrono::seconds>(tp_gameEnd-tp_gameStart).count() << " seconds.\n";

	std::cout << "Press any key to exit.";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
	std::cin.get();

	atm_gameTerminated->store(true);
}