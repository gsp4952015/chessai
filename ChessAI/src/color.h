
/*
 *
 * Console color mini-library by Jacob Tyndall [Hacktank@Hacktank.NET]
 * Usage:
 *   std::cout << sc::fgcolor(col_red) << "This text is red." << sc::fgcolorpev(); // writes colored text and reverts it back to its previous color
 *   std::cout << sc::bgcolor(col_yellow) << "This text has a yellow background." << sc::bgcolorpev(); // writes text with a colored background and reverts it back to its previous color
 * 
 * Backgorund and foreground colors can be mixed, other stream operators are sc::fgcolordef() and sc:bgcolordef(), which revert the foreground/background color to the default
 *
 **/


#ifndef __HTCONCOLOR
#define __HTCONCOLOR

#include <Windows.h>
#include <iostream>

enum {
	col_black=0,
	col_dark_blue=1,
	col_dark_green=2,

	col_dark_aqua=3,
	col_dark_cyan=3,

	col_dark_red=4,

	col_dark_purple=5,
	col_dark_pink=5,
	col_dark_magenta=5,

	col_dark_yellow=6,

	col_dark_white=7,
	col_gray=8,
	col_blue=9,
	col_green=10,

	col_aqua=11,
	col_cyan=11,

	col_red=12,

	col_purple=13,
	col_pink=13,
	col_magenta=13,

	col_yellow=14,
	col_white=15
};

unsigned char col_fg_def = col_dark_white,
			  col_bg_def = col_black,
			  col_fg_prev = col_fg_def,
			  col_bg_prev = col_bg_def;

WORD getconsoleattributes() {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&csbi);
	return csbi.wAttributes;
}

unsigned char getconsolecolor() {
	return getconsoleattributes()&0xff;
}

void setconsolefgcolor(unsigned char col_fg) {
	WORD attr = getconsoleattributes();
	col_fg_prev = attr&0xf;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),(attr&0xfff0)|col_fg);
}

void setconsolebgcolor(unsigned char col_bg) {
	WORD attr = getconsoleattributes();
	col_bg_prev = (attr&0xf0)>>4;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),(attr&0xff0f)|(col_bg << 4));
}

void setconsolecolor(unsigned char col_fg,unsigned char col_bg) {
	setconsolefgcolor(col_fg);
	setconsolebgcolor(col_bg);
}

void setconsolecolor(unsigned char col) {
	setconsolecolor((col&0xf0)>>4,col&0xf);
}

void clearconsolecolor() {
	setconsolecolor(col_fg_def,col_bg_def);
}

namespace sc {

	struct fgcolor {
		unsigned char col;
		fgcolor(unsigned char col) {
			this->col = col;
		}
		friend std::ostream& operator<<(std::ostream &l,fgcolor &r) {
			l.flush();
			setconsolefgcolor(r.col);
			return l;
		}
	};

	struct bgcolor {
		unsigned char col;
		bgcolor(unsigned char col) {
			this->col = col;
		}
		friend std::ostream& operator<<(std::ostream &l,bgcolor &r) {
			l.flush();
			setconsolebgcolor(r.col);
			return l;
		}
	};

	struct color {
		unsigned char col_fg,col_bg;
		color(unsigned char col_fg,unsigned int col_bg) {
			this->col_fg = col_fg;
			this->col_bg = col_bg;
		}
		color(const color&) = default;
		friend std::ostream& operator<<(std::ostream &l,color &r) {
			l.flush();
			setconsolecolor(r.col_fg,r.col_bg);
			return l;
		}
	};

	struct colorfgdef {
		colorfgdef() {
			//
		}
		friend std::ostream& operator<<(std::ostream &l,colorfgdef &r) {
			l.flush();
			setconsolefgcolor(col_fg_def);
			return l;
		}
	};

	struct colorbgdef {
		colorbgdef() {
			//
		}
		friend std::ostream& operator<<(std::ostream &l,colorbgdef &r) {
			l.flush();
			setconsolebgcolor(col_bg_def);
			return l;
		}
	};

	struct colordef {
		colordef() {
			//
		}
		friend std::ostream& operator<<(std::ostream &l,colordef &r) {
			l.flush();
			setconsolecolor(col_fg_def,col_bg_def);
			return l;
		}
	};

	struct colorfgprev {
		colorfgprev() {
			//
		}
		friend std::ostream& operator<<(std::ostream &l,colorfgprev &r) {
			l.flush();
			setconsolefgcolor(col_fg_prev);
			return l;
		}
	};

	struct colorbgprev {
		colorbgprev() {
			//
		}
		friend std::ostream& operator<<(std::ostream &l,colorbgprev &r) {
			l.flush();
			setconsolebgcolor(col_bg_prev);
			return l;
		}
	};

	struct colorprev {
		colorprev() {
			//
		}
		friend std::ostream& operator<<(std::ostream &l,colorprev &r) {
			l.flush();
			setconsolecolor(col_fg_prev,col_bg_prev);
			return l;
		}
	};

};

#endif
